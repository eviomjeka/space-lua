local assert = assert

local vector = require("files.lua-libs.vector")

local matrix = {}
matrix.__index = matrix

local function new(data)
	return setmetatable({ data = data or zero }, matrix)
end

local zero = new({{0, 0, 0}, 
				  {0, 0, 0}, 
				  {0, 0, 0}})
local identity = new({{1, 0, 0}, 
					  {0, 1, 0}, 
					  {0, 0, 1}})

local function ismatrix(v)
	if type(v) ~= 'table' then
		return false
	end
	if (type(v.data) ~= 'table') then
		return false
	end

	local is_matrix = true

	for i = 1, 3 do
		if (type(v.data[i]) ~= 'table') then
			return false
		end

		for j = 1, 3 do
			is_matrix = is_matrix and type(v.data[i][j]) == 'number'
		end
	end
	
	return is_matrix
end

function matrix:clone()
	local new_data = {}
	for i = 1, 3 do
		for j = 1, 3 do
			new_data[i][j] = self.data[i][j]
		end
	end

	return new(new_data)
end

function matrix:unpack()
	return	self.data[1][1], self.data[2][1], self.data[3][1], 
			self.data[1][2], self.data[2][2], self.data[3][2], 
			self.data[1][3], self.data[2][3], self.data[3][3]
end

function matrix:__tostring()
	return	"("..tonumber(self.data[1][1])..","..tonumber(self.data[1][2])..","..tonumber(self.data[1][3]).."\n"..
			" "..tonumber(self.data[2][1])..","..tonumber(self.data[2][2])..","..tonumber(self.data[2][3]).."\n"..
			" "..tonumber(self.data[3][1])..","..tonumber(self.data[3][2])..","..tonumber(self.data[3][3])..")"
end

function matrix.__unm(a)
	local new_data = {}
	for i = 1, 3 do
		for j = 1, 3 do
			new_data[i][j] = -self.data[i][j]
		end
	end

	return new(new_data)
end

function matrix.__add(a,b)
	assert(ismatrix(a) and ismatrix(b), "Add: wrong argument types (<matrix> expected)")

	local new_data = {}
	for i = 1, 3 do
		for j = 1, 3 do
			new_data[i][j] = a.data[i][j] + b.data[i][j]
		end
	end

	return new(new_data)
end

function matrix.__sub(a,b)
	assert(ismatrix(a) and ismatrix(b), "Sub: wrong argument types (<matrix> expected)")
	return martix.__add(a, matrix.__unm(b))
end

function matrix.__mul(a,b)
	if type(a) == "number" then
		return matrix.__mul(b, a)
	
	elseif type(b) == "number" then

		local new_data = {}
		for i = 1, 3 do
			for j = 1, 3 do
				new_data[i][j] = a.data[i][j] * b
			end
		end

		return new(new_data)

	elseif Vector3.isvector(b) then
		
		local data = { b.x, b.y, b.z }
		local new_data = {}
		for i = 1, 3 do
			new_data[i] = 0
			for j = 1, 3 do
				new_data[i] = new_data[i] + a.data[j][i] * data[j]
			end
		end
		
		return Vector3.new(new_data[1], new_data[2], new_data[3])
	
	else
		assert(ismatrix(a) and ismatrix(b), "Mul: wrong argument types (<matrix>, <vector> or <number> expected)")

		local new_data = {}
		for i = 1, 3 do
			for j = 1, 3 do
				new_data[i][j] = 0

				for r = 1, 3 do
					new_data[i][j] = new_data[i][j] + a.data[i][r] * b.data[r][j]
				end
			end
		end

		return new(new_data)
	end
end

function matrix.__div(a,b)
	assert(ismatrix(a) and type(b) == "number", "wrong argument types (expected <matrix> / <number>)")
	return matrix.__mul(a, 1.0 / b)
end

function matrix.__eq(a,b)
	local is_equal = true

	for i = 1, 3 do
		for j = 1, 3 do
			is_equal = is_equal and a.data[i][j] == b.data[i][j]
		end
	end

	return is_equal
end

-- the module
return setmetatable({new = new, ismatrix = ismatrix, zero = zero, identity = identity, metatable = matrix},
	{__call = function(_, ...) return new(...) end})
