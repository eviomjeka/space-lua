vector = require("files.lua-libs.vector")
matrix = require("files.lua-libs.matrix")

local t = 0

while true do
fetch_data()
end

set_engine_power(engine1, 100)
set_engine_power(engine2, 100)

while true do
	t = t + fetch_data()
	
	local self = get_self()
	local targets = read_radar()
	
	if (#targets > 0) then
		local min_k = 0
		local min_d2 = 10000000
		for k, v in pairs(targets) do
			local d2 = (v.position - self.position):len2()
			
			if v.team ~= self.team and  d2 < min_d2 then
				min_k = k
				min_d2 = d2
			end
		end

		if min_k ~= 0 then
			local dir = targets[min_k].position - self.position

			shoot(gun, dir.x, dir.y, dir.z)
		end
	end
end

