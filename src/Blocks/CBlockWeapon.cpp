#include "CBlockWeapon.h"
#include "../CWorld.h"

CBlockWeapon::CBlockWeapon(int damage, int cooldown, float bulletSpeed) :
	damage_(damage), 
	cooldown_(cooldown), 
	bulletSpeed_(bulletSpeed)
{
}

void CBlockWeapon::shoot(CBlock* block, CShipComponent* shipComponent, const btVector3& direction)
{
	int currentTime = TimeMS();
	if (currentTime - block->lastShootTime_ < GUN_COOLDOWN_MS)
		return;

	block->lastShootTime_ = currentTime;

	CBullet& bullet = WORLD->getFreeBullet();

	btVector3 localVelocity = direction.normalized() * BULLET_SPEED;
	btVector3 velocity = shipComponent->toWorldVelocity(block->getPosition()) + localVelocity;
	btVector3 position = shipComponent->toWorldPosition(block->getPosition());
	CShip* ship = shipComponent->getParent();

	bullet.Init(position, velocity, ship->color_, ship->team_);
}
