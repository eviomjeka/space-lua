#ifndef CBLOCKWEAPON
#define CBLOCKWEAPON

#include "../CBlockManager.h"
#include "../CApplication.h"

class CBlockWeapon : public CBlockBase
{
private:
    int damage_;
    int cooldown_;
    float bulletSpeed_;

public:

	CBlockWeapon(int damage, int cooldown, float bulletSpeed);

	void shoot(CBlock* block, CShipComponent* shipComponent, const btVector3& direction);

};

#endif // CBLOCKWEAPON
