#ifndef CBLOCKRADAR
#define CBLOCKRADAR

#include "../CBlockManager.h"
#include "../CShipComponent.h"
#include "../CShipController.h"

class CBlockRadar : public CBlockBase
{
private:
    float radius_;
public:

    float getRadius();
    CBlockRadar(float radius);

	void getTargets(CBlock* block, CShipComponent* shipComponent, lua_State* luaState);
};

#endif // CBLOCKRADAR
