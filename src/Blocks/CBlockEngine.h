#ifndef CBLOCKENGINE
#define CBLOCKENGINE

#include "../CBlockManager.h"
#include "../CApplication.h"

class CBlockEngine : public CBlockBase
{
private:

    float power_;

public:
    CBlockEngine(float power);
    
    void setPower(CBlock* block, CShipComponent* shipComponent, btScalar power);

};

#endif // CBLOCKENGINE
