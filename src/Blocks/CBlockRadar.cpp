#include "CBlockRadar.h"
#include "../CWorld.h"

CBlockRadar::CBlockRadar(float radius) : 
	radius_(radius)
{
}

float CBlockRadar::getRadius()
{
	return radius_;
}

void CBlockRadar::getTargets(CBlock* block, CShipComponent* shipComponent, lua_State* luaState)
{
	lua_newtable(luaState);

	for (int i = 0, j = 1; i < NUM_SHIPS; i++)
	{
		CShip* currentShip = WORLD->ship_[i];

		if (currentShip == shipComponent->getParent())
			continue;

		if (!currentShip->exist())
			continue;
		/*
		btVector3 pos1 = currentShip->body_->getCenterOfMassPosition();
		btVector3 pos2 = ship->body_->getCenterOfMassPosition();
		if ((pos1 - pos2).length() > ship->vision_)
			continue;
		*/

		lua_newtable(luaState);
		LUA_ADD_BT_VECTOR(luaState, currentShip->getComponent(0)->toWorldPosition(), "position");
		LUA_ADD_BT_VECTOR(luaState, currentShip->getComponent(0)->toWorldVelocity(btVector3(0, 0, 0)), "velocity");
		LUA_ADD_INTEGER(luaState, currentShip->getTeam(), "team");

		lua_rawseti(luaState, -2, j++);
	}
}
