#include "CBlockEngine.h"

CBlockEngine::CBlockEngine(float power) : 
	power_(power)
{
}

void CBlockEngine::setPower(CBlock* block, CShipComponent* shipComponent, btScalar power)
{
	//if (power > power_)
	//	power = power_;

	block->power_ = power;

	shipComponent->applyForceLocal(power * block->getOrientation(), block->getPosition());
}
