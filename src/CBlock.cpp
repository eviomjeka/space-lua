#include "CBlock.h"
#include "Include.h"
#include "CApplication.h"
#include "CBlockManager.h"

CBlock::CBlock()
{
	lastShootTime_ = TimeMS() - GUN_COOLDOWN_MS; // XXX: fuck this
}

CBlock::~CBlock()
{
}

void CBlock::hit(int damage)
{
    lives_ -= damage;
    if (lives_ <= 0)
    	lives_ = 0;
}

bool CBlock::exist()
{
	return lives_ > 0;
}

EBlockType CBlock::getType()
{
    return base_->getType();
}

CBlockBase* CBlock::getBase()
{
    return base_;
}

int CBlock::getLives()
{
	return lives_;
}

SIVector CBlock::getPosition()
{
	return position_;
}

SIVector CBlock::getOrientation()
{
	return orientation_;
}

void CBlock::die()
{
	lives_ = 0;
}

string& CBlock::getName()
{
	return name_;
}

float CBlock::getPower()
{
	return power_;
}

void CBlock::draw(float factor)
{
	float color_factor = 1.0f - factor;

	glColor3f(color_factor * (color_.x * 1.0f + (1.0f - color_.x * 1.0f) * (base_->getLives() - lives_) / base_->getLives()),
			  color_factor * (color_.y * 1.0f + (1.0f - color_.y * 1.0f) * (base_->getLives() - lives_) / base_->getLives()),
			  color_factor * (color_.z * 1.0f + (1.0f - color_.z * 1.0f) * (base_->getLives() - lives_) / base_->getLives()));

	SIVector2D textureCoord = base_->getTextureCoord();

	glBegin(GL_QUADS);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(textureCoord.x / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(-0.5f, 0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, 0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(0.5f, 0.5f, -0.5f);
	glTexCoord2f(textureCoord.x / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, 0.5f, -0.5f);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(textureCoord.x / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, -0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, 0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, 0.5f, 0.5f);
	glTexCoord2f(textureCoord.x / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, -0.5f, 0.5f);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(textureCoord.x / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, 0.5f, -0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, 0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(0.5f, -0.5f, 0.5f);
	glTexCoord2f(textureCoord.x / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(0.5f, -0.5f, -0.5f);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glTexCoord2f(textureCoord.x / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(-0.5f, -0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(-0.5f, 0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, 0.5f, -0.5f);
	glTexCoord2f(textureCoord.x / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, -0.5f, -0.5f);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3f(0.0f, -1.0f, 0.0f);
	glTexCoord2f(textureCoord.x / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, -0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(-0.5f, -0.5f, 0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, -0.5f, -0.5f);
	glTexCoord2f(textureCoord.x / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(0.5f, -0.5f, -0.5f);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, -1.0f);
	glTexCoord2f(textureCoord.x / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, 0.5f, -0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, textureCoord.y / 16.0f);
	glVertex3f(0.5f, -0.5f, -0.5f);
	glTexCoord2f((textureCoord.x + 1) / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, -0.5f, -0.5f);
	glTexCoord2f(textureCoord.x / 16.0f, (textureCoord.y + 1) / 16.0f);
	glVertex3f(-0.5f, 0.5f, -0.5f);
	glEnd();
}
