#include "CShipController.h"
#include "CShip.h"
#include "Blocks/CBlockRadar.h"
#include "Blocks/CBlockWeapon.h"
#include "Blocks/CBlockEngine.h"

char CShipController::this_key_ = 0;

int get_self(lua_State* luaState) { return 0; }
int read_radar(lua_State* luaState) { return 0; }

void yield_break(lua_State* luaState, lua_Debug*)
{
	CShipController::getByLuaState(luaState)->completedStep_ = false;
	//TODO: Fines
	if (lua_isyieldable(luaState))
		lua_yield(luaState, 0);
}

CShipController::CShipController(CShip* parent, const char* script) :
	parent_(parent),
	luaState_(NULL),
	luaThreadState_(NULL)
{
	luaState_ = luaL_newstate();
	luaL_openlibs(luaState_);

	LUA_REGISTER_FUNCTION(luaState_, readRadar, "read_radar")
	LUA_REGISTER_FUNCTION(luaState_, shoot, "shoot")
	LUA_REGISTER_FUNCTION(luaState_, yield, "fetch_data")
	LUA_REGISTER_FUNCTION(luaState_, setEnginePower, "set_engine_power")
	LUA_REGISTER_FUNCTION(luaState_, dumpShip, "get_self")

	luaThreadState_ = lua_newthread(luaState_);
	luaL_loadfile(luaThreadState_, script);
	lua_sethook(luaThreadState_, yield_break, LUA_MASKCOUNT, INSTRUCTION_LIMIT);

	lua_pushlightuserdata(luaState_, (void*) &this_key_);
	lua_pushlightuserdata(luaState_, (void*) this);
	lua_settable(luaState_, LUA_REGISTRYINDEX);
}

void CShipController::stop()
{
	lua_close(luaState_);
	luaState_ = NULL;
	luaThreadState_ = NULL;
}

bool CShipController::isRunning()
{
	if (!luaState_)
		ASSERT(parent_->exist());

	return luaState_ != NULL;
}

void CShipController::doStep(float dt)
{
	if (!isRunning())
		return;

	lua_pushnumber(luaThreadState_, dt);
	int result = lua_resume(luaThreadState_, NULL, 1);

	if (result != LUA_OK && result != LUA_YIELD)
	{
		fprintf(stderr, "LUA error: %s\n", lua_tostring(luaThreadState_, -1));
		lua_pop(luaThreadState_, 1);

		ASSERT(false);
	}

	if (result == LUA_OK)
		stop();
}

void CShipController::registerBlock(int component, int block, string name)
{
	lua_newtable(luaState_);
	LUA_ADD_INTEGER(luaState_, component, "component");
	LUA_ADD_INTEGER(luaState_, block, "block");
	lua_setglobal(luaState_, name.c_str());
}

CShipController::~CShipController()
{
	if (isRunning())
		stop();
}

CShipController* CShipController::getByLuaState(lua_State* luaState)
{
	lua_pushlightuserdata(luaState, (void*) &this_key_);
	lua_gettable(luaState, LUA_REGISTRYINDEX);
	void* ship = lua_touserdata(luaState, -1);
	ASSERT(ship);

	return (CShipController*) ship;
}

btVector3 CShipController::getVector_(int param)
{
	lua_getfield(luaThreadState_, param, "x");
	lua_getfield(luaThreadState_, param, "y");
	lua_getfield(luaThreadState_, param, "z");

	btVector3 v((btScalar) lua_tonumber(luaThreadState_, -3),
				(btScalar) lua_tonumber(luaThreadState_, -2),
				(btScalar) lua_tonumber(luaThreadState_, -1));

	lua_pop(luaThreadState_, 3);
	return v;
}


CBlock* CShipController::getBlock_(int param, CShipComponent** _component)
{
	lua_getfield(luaThreadState_, param, "component");
	lua_getfield(luaThreadState_, param, "block");
	int componentID = (int) lua_tointeger(luaThreadState_, -2);
	int blockID     = (int) lua_tointeger(luaThreadState_, -1);
	lua_pop(luaThreadState_, 2);

	CShipComponent* component = parent_->getComponent(componentID);
	if (!component || !component->doesExist())
		return NULL;

	CBlock* block = component->getBlock(blockID);
	if (!block || !block->exist())
		return NULL;

	if (_component)
		*_component = component;
	
	return block;
}

int CShipController::yield()
{
	completedStep_ = true;

	lua_yield(luaThreadState_, 0);

	return 0;
}

int CShipController::dumpShip()
{
	parent_->dumpState(luaThreadState_);

	return 1;
}

int CShipController::shoot()
{
	CShipComponent* component = NULL;
	CBlock* block = getBlock_(1, &component);

	if (block && block->getType() == BlockWeapon)
		((CBlockWeapon*)block->getBase())->shoot(block, component, getVector_(2));

	return 0;
}

int CShipController::setEnginePower()
{
	CShipComponent* component = NULL;
	CBlock* block = getBlock_(1, &component);
	btScalar enginePower = (btScalar) lua_tonumber(luaThreadState_, 2);

	if (block && block->getType() == BlockEngine)
		((CBlockEngine*) block->getBase())->setPower(block, component, enginePower);

	return 0;
}

int CShipController::readRadar()
{
	CShipComponent* component = NULL;
	CBlock* block = getBlock_(1, &component);

	if (block && block->getType() == BlockRadar)
	{
		((CBlockRadar*)block->getBase())->getTargets(block, component, luaThreadState_);

		return 1;
	}

	return 0;
}


/*
void yield_break(lua_State* luaState, lua_Debug*)
{
	CShip::getShipByLuaState(luaState)->completedStep_ = false;
	//TODO: Fines
	if (lua_isyieldable(luaState))
		lua_yield(luaState, 0);
}
*/