#include "CApplication.h"
#include "Utilities/Misc.h"
#include "Utilities/CTexture.h"
#include "Utilities/CPngImage.h"
#include "CWorld.h"

CApplication* CApplication::instance = NULL;

const int CApplication::WINDOW_WIDTH		 = 800;
const int CApplication::WINDOW_HEIGHT		 = 600;
const int CApplication::MIN_WINDOW_WIDTH	 = 700;
const int CApplication::MIN_WINDOW_HEIGHT	 = 500;

CApplication::CApplication()
{}

CApplication::~CApplication() {}

void CApplication::init()
{
	instance = this;

	int time = TimeMS();
	CFileSystem::CreateDir("logs");
	initLog_();

	setOpenGLParameters_();

	createWindow(false, SIVector2D (WINDOW_WIDTH, WINDOW_HEIGHT), SIVector2D (MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));
	printOpenGLInformation_();

	LOG("%dms - Window created", TimeMS() - time); time = TimeMS();

	showCursor(false);

	font_.Init();

	world_ = new CWorld;
	world_->init();

	LOG("APPLICATION INITIALIZED");
}

void CApplication::initLog_()
{
	CLog::Instance.Init();

	__LOG ("Space LUA\n");

	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);
	char str[LOG_STR_SIZE] = "";
	strftime (str, LOG_STR_SIZE - 1, "Launch time: [%d-%m-%y %H.%M.%S]\n", timeinfo);
	__LOG (str);

	__LOG ("Last line of log should be: \"APPLICATION DESTROYED\". "
				   "If the last line of the other, the application not completed successfully.\n\n");

	LOG ("APPLICATION IS INITIALIZED");
}

void CApplication::destroy()
{
	LOG("DESTROYING APPLICATION"); // TODO: camera fix

	world_->destroy();
    delete world_;

	font_.Destroy();

	LOG("APPLICATION DESTROYED");

	CLog::Instance.Destroy();
}

void CApplication::proc(int time)
{
	glassert();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	perspective_(70.0f, APPLICATION->getWindowSize().x / (float) APPLICATION->getWindowSize().y, 1.0f, 2500.0f);
	glMatrixMode(GL_MODELVIEW);

	world_->proc(time);
}

void CApplication::perspective_(float fovy, float aspect, float zNear, float zFar)
		{
	float yMax = zNear * tan (fovy * PI / 360.0f);
	float yMin = -yMax;
	float xMin = yMin * aspect;
	float xMax = yMax * aspect;

	glFrustum(xMin, xMax, yMin, yMax, zNear, zFar);
}

void CApplication::keyDown(unsigned key)
{
	eventDown_(key);
}

void CApplication::keyUp(unsigned key)
{
	eventUp_(key);
}

void CApplication::leftButtonDown()
{
	eventDown_(MOUSE_LEFT_BUTTON);
}

void CApplication::leftButtonUp()
{
	eventUp_(MOUSE_LEFT_BUTTON);
}

void CApplication::rightButtonDown()
{
	eventDown_(MOUSE_RIGHT_BUTTON);
}

void CApplication::rightButtonUp()
{
	eventUp_(MOUSE_RIGHT_BUTTON);
}

void CApplication::mouseWheel(int delta)
{
	if (delta > 0)
	{
		for (int i = 0; i < delta; i++)
			eventDown_(MOUSE_WHEEL_UP);
	}
	else
	{
		for (int i = 0; i < -delta; i++)
			eventDown_(MOUSE_WHEEL_DOWN);
	}
}

void CApplication::eventDown_(unsigned key)
{
	if (key == KEY_W)		world_->moveDirection(MOVE_DIRECTION_HIGH_Z, true);
	else if (key == KEY_S)	world_->moveDirection(MOVE_DIRECTION_LOW_Z, true);
	else if (key == KEY_D)	world_->moveDirection(MOVE_DIRECTION_HIGH_X, true);
	else if (key == KEY_A)	world_->moveDirection(MOVE_DIRECTION_LOW_X, true);
	else if (key == KEY_E)	world_->moveDirection(MOVE_DIRECTION_HIGH_Y, true);
	else if (key == KEY_Q)	world_->moveDirection(MOVE_DIRECTION_LOW_Y, true);
}

void CApplication::eventUp_(unsigned key)
{
	if (key == KEY_W)		world_->moveDirection(MOVE_DIRECTION_HIGH_Z, false);
	else if (key == KEY_S)	world_->moveDirection(MOVE_DIRECTION_LOW_Z, false);
	else if (key == KEY_D)	world_->moveDirection(MOVE_DIRECTION_HIGH_X, false);
	else if (key == KEY_A)	world_->moveDirection(MOVE_DIRECTION_LOW_X, false);
	else if (key == KEY_E)	world_->moveDirection(MOVE_DIRECTION_HIGH_Y, false);
	else if (key == KEY_Q)	world_->moveDirection(MOVE_DIRECTION_LOW_Y, false);
	else if (key == KEY_ESCAPE)
	{
		fullscreenMode(false); // TODO: to CWindows
		destroyWindow();
	}
	else if (key == KEY_F11)
		fullscreenMode(!isFullScreen());
}

void CApplication::mouseMotion(SIVector2D position)
{
	static SIVector2D prevPosition = position; // TODO: use variable

	SFVector2D delta = ((SFVector2D) position - prevPosition) * 0.3f;

	world_->rotate(SFVector2D(delta.y, delta.x)); // TODO: wtf?

	prevPosition = position;

	SFVector2D fromCenter = (SFVector2D) position -((SFVector2D) getWindowSize()) / 2.0f;
	if (fabs(fromCenter.x / getWindowSize().x) >= 0.25 || fabs(fromCenter.y / getWindowSize().y) >= 0.25)
	{
		prevPosition = getWindowSize() / 2;
		setCursorPosition(prevPosition);
	}
}

void CApplication::resize()
{
	glViewport(0, 0, getWindowSize().x, getWindowSize().y);
}

void CApplication::unfocus()
{
}

void CApplication::setOpenGLParameters_()
{
	setRGBA();
	setMinimumColorSize(8, 8, 8, 8);
	setDoubleBuffer();
	setMinimumDepthSize(24);

	requestMSAA(4);
}

void CApplication::printOpenGLInformation_()
{
	LOG("OpenGL information:");
	LOG("Vendor: %s", glGetString(GL_VENDOR));
	LOG("Renderer: %s", glGetString(GL_RENDERER));
	LOG("OpenGL version: %s", glGetString(GL_VERSION));
	LOG("GLSL version: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));
	LOG("Extensions: %s", glGetString(GL_EXTENSIONS));

	glassert();
}

CWorld* CApplication::getWorld()
{
	return world_;
}

CFont* CApplication::getFont()
{
	return &font_;
}
