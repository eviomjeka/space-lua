#include "CShip.h"
#include "Include.h"
#include "CApplication.h"

CShip::CShip(const char* script, const char* construction, SFVector color, SFVector position, int team) :
	shipController_(this, script),
	color_(color),
	team_(team),
	mainComponent_(-1)
{
	btQuaternion rotation((rand() - RAND_MAX) / 2.0f,
						  (rand() - RAND_MAX) / 2.0f,
						  (rand() - RAND_MAX) / 2.0f,
					      (M_PI * rand()) / RAND_MAX);

	btTransform transform(rotation, position);

	initComponents_(construction, transform);
}

CShipComponent* CShip::getComponentByPosition(SIVector position)
{
	for (auto component : components_)
		if (component->getBlockByLocalPosition(position))
			return component;

	return NULL;
}

void CShip::initComponents_(const char* contruction, const btTransform& transform)
{
	CFile constr(contruction, CFILE_READ);

	while (!constr.EndOfFile())
	{
		char name[256];
		constr.Scan("~%s", name);
		components_.push_back(new CShipComponent(this, components_.size(), name, constr, transform, color_));
		constr.Scan(" ");
	}

	constr.Destroy();
}

void CShip::die()
{
	if (!exist())
		return;

	for (auto component : components_)
		if (component->doesExist())
			component->die();
}


CShip::~CShip()
{
	if (exist())
		die();
}

bool CShip::exist()
{
	return components_[0]->doesExist();
}



void CShip::doStep(float dt)
{
	if (!exist())
		return;

	shipController_.doStep(dt);
}


void CShip::draw()
{
	if (!exist())
		return;

	for (auto component : components_)
		component->draw();
}

void CShip::prepareForPhysicsStep()
{
	if (!exist())
		return;

	for (auto component : components_)
		component->prepareForPhysicsStep();
}

CShipComponent* CShip::getComponent(unsigned ID)
{
	if (ID >= components_.size())
	return NULL;

	return components_[ID];
		}

CShipController* CShip::getController()
		{
	return &shipController_;
		}

void CShip::dumpState(lua_State* luaState)
		{
	//TODO: uncomment

	lua_newtable(luaState);

	LUA_ADD_NUMBER(luaState, 1, "size")
	LUA_ADD_INTEGER(luaState, team_, "team")

	for (auto& component : components_)
		if (component->doesExist())
			component->dumpState(luaState);
}

int CShip::getTeam()
{
	return team_;
}
