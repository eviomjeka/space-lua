#ifndef CWORLD
#define CWORLD

#include "Include.h"
#include "CApplication.h"
#include "CBullet.h"

class CShip;

#define NUM_SHIPS_PER_TEAM	3
#define NUM_TEAMS			3
#define NUM_SHIPS			NUM_SHIPS_PER_TEAM * NUM_TEAMS

#define NUM_STARS			500
#define GRAVITY_CONSTANT	0.0f
#define TIME_STEP			0.1f

class CWorld
{
public:

	void init();
	void destroy();

	void proc(int time);
	void moveDirection(int Direction, bool Move);
	void rotate(SFVector2D delta);
	CBullet& getFreeBullet();

	btDiscreteDynamicsWorld* world_; // TODO: remove
	btBoxShape*	boxShape_;
	btSphereShape* sphereShape_;
	CBlockManager* blockManager_;
	CParticleSystem* particleSystem_;
	unsigned blocks_;
	CBullet bullets_[MAX_BULLETS];
	CShip* ship_[NUM_SHIPS];

private:

	void DrawSkydome_();

	SFVector2D Orientation_;
	SFVector Position_;
	SFVector Velocity_;
	SFVector stars_[NUM_STARS];
	btBroadphaseInterface* broadphase_;
	btDefaultCollisionConfiguration* collisionConfiguration_;
	btCollisionDispatcher* dispatcher_;
	btSequentialImpulseConstraintSolver* solver_;

	friend bool check_collision(CShip* ship);
	friend int read_radar(lua_State* luaState);
	friend void internalBulletTickCallback(btDynamicsWorld *world, btScalar timeStep);
	friend void internalBulletTickCallbackPre(btDynamicsWorld *world, btScalar timeStep);

};

#endif // CWORLD
