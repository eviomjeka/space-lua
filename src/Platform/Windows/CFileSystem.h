﻿#ifndef CFILESYSTEM
#define CFILESYSTEM

#include "../../Utilities/CArray.h"
#include <PlatformMisc.h>

enum FileSystem_RenameDir
{
	FILESYSTEM_RENAMEDIR_SUCCESS,
	FILESYSTEM_RENAMEDIR_NOTFOUND,
	FILESYSTEM_RENAMEDIR_EXISTS
};

struct CFileSystemElement
{
	std::string Name;
	bool Directory;
};

class CFileSystem
{
public:

	static void FileList (std::vector<CFileSystemElement>* FilesArray, const char* Path);
	static bool CreateDir (const char* Path);
	static bool DeleteDir (const char* Path);
	static bool DeleteFile (const char* Path);
	static char RenameDir (const char* Src, const char* Dest);
	static char* ToGameFolderPath (const char* Path, char* GamePath);

private:
	CFileSystem();
};

#endif