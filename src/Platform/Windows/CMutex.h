﻿#ifndef CMUTEX
#define CMUTEX

#include <PlatformMisc.h>

class CMutex
{
private:
	CRITICAL_SECTION	Mutex_;
	bool				Locked_;
	
public:
	CMutex();
	~CMutex();
	
	void Lock();
	void Unlock();
	bool TryLock();

};

#endif