﻿#ifndef PLATFORMINCLUDE
#define PLATFORMINCLUDE

#if RELEASE == FALSE
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <windows.h>
#include "resource.h"

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cstdarg>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/wglew.h>
#include <zlib.h>
#include <png.h>

#endif