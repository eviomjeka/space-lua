﻿#ifndef PROGRAMERROR
#define PROGRAMERROR

#include <PlatformMisc.h>

#if ASSERTS == ASSERTS_ALL || ASSERTS == ASSERTS_NORMAL

#define PROGRAM_ERROR_STR_SIZE (1 << 15)

// Error!
// ====================================
// [Reason1]: { [SReason2] ? "[SReason2]" : "[IReason2]" }
// { [Description] ? "Description: [Description]" : "" }
// File: [File]
// Function: [Function]
// Line: [Line]
// { [AddictionalErrorCodes] ? "errno: ... (...)" : "" }
// { [AddictionalErrorCodes] ? "GetLastError: ... (...)" : "" }
// ====================================

void __Error (const char* Reason1, const char* SReason2, 
			  const int IReason2, const char* Description, 
			  const char* File, const char* Function, 
			  const int Line, const bool AddictionalErrorCodes);

extern char __ErrorString[PROGRAM_ERROR_STR_SIZE];
// ^ we can't use local variable because they uses stack

#define ASSERT(OK)												\
{																\
	if (!(OK))													\
		__Error ("Assertation failed", #OK, 0, NULL,			\
				 __FILE__, __FUNCSIG__, __LINE__, true);		\
}

#define glassert()												\
{																\
	int OpenGLError = glGetError();								\
	if (OpenGLError)											\
		__Error ("OpenGL error", NULL, OpenGLError,	NULL, 		\
				 __FILE__, __FUNCSIG__, __LINE__, false);		\
}


#define alassert()												\
{																\
	int OpenALError = alGetError();								\
	if (OpenALError)											\
		__Error ("OpenAL error", NULL, OpenALError,				\
				 alGetString (OpenALError), 					\
				 __FILE__, __FUNCSIG__, __LINE__, false);		\
																\
	int ALCError =  alcGetError (COpenAL::Device());			\
	if (OpenALError)											\
		__Error ("OpenAL error", NULL, OpenALError,				\
				 alcGetString (COpenAL::Device(), ALCError), 	\
				 __FILE__, __FUNCSIG__, __LINE__, false);		\
}

#define error(Format, ...)										\
{																\
	snprintf (__ErrorString, PROGRAM_ERROR_STR_SIZE,			\
			  Format, __VA_ARGS__);								\
																\
	__Error ("Program error", __ErrorString, 0, NULL,			\
			 __FILE__, __FUNCSIG__, __LINE__, false);			\
}

#if ASSERTS == ASSERTS_ALL
#define __ASSERT(ok) ASSERT(ok)
#else
#define __ASSERT(ok) (ok)
#endif

#else

#pragma warning (disable: 4390) // TODO: fix, use empty function

#define ASSERT(OK) (OK)
#define __ASSERT(OK) (OK)
#define glassert()
#define alassert()
#define error(Text, ...)

#endif

#endif