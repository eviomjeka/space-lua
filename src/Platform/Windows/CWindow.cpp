﻿#include "CWindow.h"

CWindow* __CWindow;

CWindow::CWindow()
{}

CWindow::~CWindow()
{}

bool CWindow::createWindow(bool fullscreen, SIVector2D windowSize, SIVector2D minWindowSize)
{
	__CWindow = this;

	hideCursor_ = false;
	focused_	= true;
	fullscreen_	= fullscreen;
	destroy_	= false;

	minWindowSize_ = minWindowSize;
	prevousCursorPosition_ = SIVector2D(-1, -1);

	WSADATA data = {};
	ASSERT (WSAStartup(MAKEWORD(2, 2), (WSADATA*) &data) == 0);

	char windowClassName[256];
	sprintf(windowClassName, "%s_Window::Class", APPLICATION_NAME);

	WNDCLASSEX wndClass = {};
	wndClass.cbSize			= sizeof (WNDCLASSEX);
	wndClass.style			= CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc	= wndProc_;
	wndClass.cbClsExtra		= 0;
	wndClass.cbWndExtra		= 0;
	wndClass.hInstance		= GetModuleHandle(NULL);
	wndClass.hIcon			= LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON));
	wndClass.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName	= NULL;
	wndClass.lpszClassName	= windowClassName;

	ASSERT (RegisterClassEx(&wndClass) != NULL);
	
	PIXELFORMATDESCRIPTOR pixelFormatDescriptor = {};
	pixelFormatDescriptor.nSize      = sizeof (PIXELFORMATDESCRIPTOR);
	pixelFormatDescriptor.nVersion   = 1;
	pixelFormatDescriptor.dwFlags    = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_GENERIC_ACCELERATED;
	pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits = 16;
	pixelFormatDescriptor.cDepthBits = 32;

	initGLEW_(windowClassName, &pixelFormatDescriptor);

	windowBorderSize_ = SIVector2D (2 * GetSystemMetrics(SM_CXFRAME), 
		GetSystemMetrics(SM_CYCAPTION) + 2 * GetSystemMetrics(SM_CYFRAME));
	
	int style = 0;
	if (!fullscreen_)
	{
		style = WS_OVERLAPPEDWINDOW;
		windowSize_		= windowSize + windowBorderSize_;
		windowPosition_	= SIVector2D((GetSystemMetrics (SM_CXSCREEN) - windowSize.x) / 2,
									 (GetSystemMetrics (SM_CYSCREEN) - windowSize.y) / 2);
	}
	else
	{
		style = WS_POPUP;
		windowSize_		= SIVector2D(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
		windowPosition_	= SIVector2D(0, 0);

		windowSize					= windowSize + windowBorderSize_;
		noFullscreenWindowSize_		= windowSize;
		noFullscreenWindowPosition_	= SIVector2D((GetSystemMetrics(SM_CXSCREEN) - windowSize.x) / 2,
												 (GetSystemMetrics(SM_CYSCREEN) - windowSize.y) / 2);
		
		windowSize = windowSize_;
	}

	window_ = CreateWindowA(windowClassName, APPLICATION_NAME, style, windowPosition_.x, windowPosition_.y,
							windowSize.x, windowSize.y, NULL, (HMENU) NULL, GetModuleHandle(NULL), NULL);
	ASSERT(window_ != NULL);
	
	DC_ = GetDC(window_);
	ASSERT(DC_);

	int attributes[] = {
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
		WGL_COLOR_BITS_ARB, 24,
		WGL_ALPHA_BITS_ARB, 8,
		WGL_DEPTH_BITS_ARB, 16,
		WGL_STENCIL_BITS_ARB, 0,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
		WGL_SAMPLES_ARB, 1 /* TODO: wtf? */, 
		0, 0 };
	
	int pixelFormat = 0;
	UINT numFormats = 0;
	ASSERT (wglChoosePixelFormatARB);
	if (!wglChoosePixelFormatARB(DC_, attributes, NULL, 1, &pixelFormat, &numFormats))
	{
		// no multisampling
		int attributes2[] = {
			WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
			WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
			WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
			WGL_COLOR_BITS_ARB, 24,
			WGL_ALPHA_BITS_ARB, 8,
			WGL_DEPTH_BITS_ARB, 16,
			WGL_STENCIL_BITS_ARB, 0,
			WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
			0, 0 };

		ASSERT(wglChoosePixelFormatARB(DC_, attributes2, NULL, 1, &pixelFormat, &numFormats));
		
		WARNING("Multisampling doesn't supported");
	}
	ASSERT(numFormats >= 1);
	
	ASSERT(SetPixelFormat(DC_, pixelFormat, &pixelFormatDescriptor));
	
	RC_ = wglCreateContext(DC_);
	ASSERT(RC_);
	ASSERT(wglMakeCurrent (DC_, RC_));

#if RELEASE == FALSE
	if (wglSwapIntervalEXT)
	{
		ASSERT(wglSwapIntervalEXT(0));
	}
	else
		WARNING("wglSwapIntervalEXT doesn't supported");
#endif
	
	ShowWindow(window_, SW_SHOW);
	ASSERT(SetFocus(window_));

	resize();

	return true;
}

void CWindow::initGLEW_(std::string windowClassName, PIXELFORMATDESCRIPTOR* pixelFormatDescriptor)
{
	ignoreResize_ = true;
	window_ = CreateWindowA(windowClassName.c_str(), APPLICATION_NAME, WS_POPUP, 0, 0,
							1, 1, NULL, (HMENU) NULL, GetModuleHandle (NULL), NULL);
	ASSERT (window_ != NULL);
	ignoreResize_ = false;

	DC_ = GetDC(window_);
	ASSERT(DC_);

	int pixelFormat = ChoosePixelFormat(DC_, pixelFormatDescriptor);
	ASSERT(pixelFormat);
	ASSERT(SetPixelFormat(DC_, pixelFormat, pixelFormatDescriptor));

	RC_ = wglCreateContext(DC_);
	ASSERT(RC_);
	ASSERT(wglMakeCurrent(DC_, RC_));

	GLenum errorCode = glewInit();
	if (errorCode != GLEW_OK)
		error("%s", (const char*) glewGetErrorString(errorCode));
	ASSERT(glewIsSupported("GL_VERSION_2_0"));
	ASSERT(glewIsSupported("GL_ARB_vertex_buffer_object")); // TODO: remove? + add more checks
	glassert();

	ASSERT(wglMakeCurrent (NULL, NULL));
	ASSERT(wglDeleteContext (RC_));
	ASSERT(ReleaseDC(window_, DC_));

	ASSERT(::DestroyWindow(window_));
}

void CWindow::mainLoop()
{
	ASSERT (__CWindow == this);

	MSG message = {};
	int time = timeGetTime();

	while (!destroy_)
	{
		if (PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}
		else
		{
			int newTime = timeGetTime();
			proc(newTime - time);
			SwapBuffers(DC_);
			time = newTime;
		}
	}
	
	destroy();
	
	ASSERT(wglMakeCurrent(NULL, NULL));
	ASSERT(wglDeleteContext(RC_));
	ASSERT(ReleaseDC(window_, DC_));

	ASSERT(::DestroyWindow(window_));
}

//==============================================================================

SIVector2D CWindow::getWindowSize()
{
	return windowSize_;
}

void CWindow::setCursorPosition(SIVector2D position)
{
	if (!focused_)
		return;

	if (fullscreen_)
		SetCursorPos(position.x, position.y);
	else
		SetCursorPos(position.x + windowPosition_.x, position.y + windowPosition_.y);
}

void CWindow::showCursor(bool show)
{
	if (show && hideCursor_)
	{
		hideCursor_ = false;
		if (focused_)
			ASSERT(ClipCursor(NULL));
		while (::ShowCursor(true) < 0);
	}
	else if (!show && !hideCursor_)
	{
		hideCursor_ = true;
		if (focused_)
		{
			RECT rect = { windowPosition_.x, windowPosition_.y, 
				windowPosition_.x + windowSize_.x, windowPosition_.y + windowSize_.y };
			ASSERT (ClipCursor (&rect));
		}
		while (::ShowCursor(false) >= 0);
	}
}

bool CWindow::isFullScreen()
{
	return fullscreen_;
}

void CWindow::fullscreenMode(bool fullscreen)
{
	if (fullscreen == fullscreen_)
		return;

	fullscreen_ = fullscreen;

	if (!fullscreen)
	{
		ignoreResize_ = true;
		ASSERT(SetWindowLong(window_, GWL_STYLE, WS_OVERLAPPEDWINDOW));
		ASSERT(SetWindowPos(window_, HWND_TOP, 0, 0, 0, 0, 
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED));
		ignoreResize_ = false;

		windowSize_		= noFullscreenWindowSize_;
		windowPosition_	= noFullscreenWindowPosition_;
		
		SIVector2D windowSize = windowSize_ + windowBorderSize_;
		ASSERT(SetWindowPos(window_, HWND_TOP, windowPosition_.x, 
			   windowPosition_.y, windowSize.x, windowSize.y, 0));
	}
	else
	{
		ignoreResize_ = true;
		ASSERT(SetWindowLong(window_, GWL_STYLE, WS_POPUP));
		ASSERT(SetWindowPos(window_, HWND_TOP, 0, 0, 0, 0, 
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED));
		ignoreResize_ = false;

		noFullscreenWindowSize_		= windowSize_;
		noFullscreenWindowPosition_	= windowPosition_;
		windowSize_		= SIVector2D(GetSystemMetrics (SM_CXSCREEN), GetSystemMetrics (SM_CYSCREEN));
		windowPosition_	= SIVector2D(0, 0);

		ASSERT(SetWindowPos(window_, HWND_TOP, windowPosition_.x, 
			   windowPosition_.y, windowSize_.x, windowSize_.y, 0));
	}
	
	ShowWindow(window_, SW_SHOW);
	resize();

	prevousCursorPosition_ = windowSize_ / 2;
	setCursorPosition(prevousCursorPosition_);
	mouseMotion(prevousCursorPosition_);

	if (hideCursor_)
	{
		RECT rect = { windowPosition_.x, windowPosition_.y, 
			windowPosition_.x + windowSize_.x, windowPosition_.y + windowSize_.y };
		ASSERT(ClipCursor(&rect));
	}
}

void CWindow::destroyWindow()
{
	destroy_ = true;
}

void CWindow::setRGBA()
{
	
}

void CWindow::setMinimumColorSize (int, int, int, int)
{
	
}

void CWindow::setDoubleBuffer()
{

}
void CWindow::setMinimumDepthSize (int)
{

}

void CWindow::requestMSAA (int)
{

}

LRESULT CALLBACK CWindow::wndProc_(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_GETMINMAXINFO:
		{
			MINMAXINFO* info = (MINMAXINFO*) lParam;
			POINT min = { __CWindow->minWindowSize_.x + __CWindow->windowBorderSize_.x, 
						  __CWindow->minWindowSize_.y + __CWindow->windowBorderSize_.y };
			POINT max = { GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN) };
			info->ptMinTrackSize = min;
			info->ptMaxTrackSize = max;
		}
		return 0;

		case WM_SYSCOMMAND:
			switch (wParam)
			{
				case SC_SCREENSAVE:		
				case SC_MONITORPOWER:	
					return 0;
			}
		break;

		case WM_KEYDOWN:
			if ((lParam & 0x40000000) == 0)
				__CWindow->keyDown((unsigned) wParam);
		return 0;

		case WM_KEYUP:
			__CWindow->keyUp((unsigned) wParam);
		return 0;

		case WM_LBUTTONDOWN:
			__CWindow->leftButtonDown();
		return 0;

		case WM_LBUTTONUP:
			__CWindow->leftButtonUp();
		return 0;

		case WM_RBUTTONDOWN:
			__CWindow->rightButtonDown();
		return 0;

		case WM_RBUTTONUP:
			__CWindow->rightButtonUp();
		return 0;

		case WM_MOUSEWHEEL:
		{
			int delta = GET_WHEEL_DELTA_WPARAM(wParam);
			__CWindow->mouseWheel(-delta / WHEEL_DELTA);
		}
		return 0;

		case WM_MOVE:
			__CWindow->windowPosition_.x = LOWORD(lParam);
			__CWindow->windowPosition_.y = HIWORD(lParam);
		return 0;

		case WM_MOUSEMOVE:
			if (__CWindow->prevousCursorPosition_.x != LOWORD(lParam) || 
				__CWindow->prevousCursorPosition_.y != HIWORD(lParam))
			{
				__CWindow->prevousCursorPosition_ = SIVector2D(LOWORD(lParam), HIWORD(lParam));
				__CWindow->mouseMotion(SIVector2D(LOWORD(lParam), HIWORD(lParam)));
			}
		return 0;

		case WM_SIZE:
			if ((LOWORD(lParam) != 0 && HIWORD(lParam) != 0) && !__CWindow->ignoreResize_ && 
				(LOWORD(lParam) != __CWindow->windowSize_.x || HIWORD(lParam) != __CWindow->windowSize_.y))
			{
				if (__CWindow->hideCursor_)
				{
					RECT rect = { __CWindow->windowPosition_.x, __CWindow->windowPosition_.y, 
								  __CWindow->windowPosition_.x + __CWindow->windowSize_.x, 
								  __CWindow->windowPosition_.y + __CWindow->windowSize_.y };
					ASSERT (ClipCursor(&rect));
				}

				__CWindow->windowSize_.x = LOWORD(lParam);
				__CWindow->windowSize_.y = HIWORD(lParam);
				__CWindow->resize();
			}
		return 0;

 		case WM_CLOSE:
			__CWindow->destroy_ = true;
		return 0;

		case WM_ACTIVATE:
			if (LOWORD(wParam) != WA_INACTIVE && HIWORD(wParam) == 0)
			{
				__CWindow->focused_ = true;
				if (__CWindow->hideCursor_)
				{
					RECT rect = { __CWindow->windowPosition_.x, __CWindow->windowPosition_.y, 
								  __CWindow->windowPosition_.x + __CWindow->windowSize_.x, 
								  __CWindow->windowPosition_.y + __CWindow->windowSize_.y };
					ASSERT (ClipCursor(&rect));
				}
			}
			else
			{
				__CWindow->focused_ = false;
				if (__CWindow->hideCursor_)
				{
					ASSERT(ClipCursor(NULL));
				}
				__CWindow->unfocus();
			}
		return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(window, message, wParam, lParam);
}
