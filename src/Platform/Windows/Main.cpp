﻿#include "../../CApplication.h"

#if RELEASE == FALSE

int main(int argc, char** argv)
{
	//_CrtSetDbgFlag (_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_CHECK_ALWAYS_DF);
	//_CrtSetBreakAlloc ();

	CApplication* application = new CApplication;
	
	application->init();
	application->mainLoop();
	
	delete application;

	return 0;
}

#else

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	CApplication* application = new CApplication;
	
	application->init();
	application->mainLoop();

	delete application;

	return 0;
}

#endif