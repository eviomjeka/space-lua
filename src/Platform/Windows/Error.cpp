#include "Error.h"
#include "../../Utilities/CLog.h"

char __ErrorString[PROGRAM_ERROR_STR_SIZE] = "";

void __Error (const char* Reason1, const char* SReason2, 
			  const int IReason2, const char* Description, 
			  const char* File, const char* Function, 
			  const int Line, const bool AddictionalErrorCodes)
{
	int ErrNo		= AddictionalErrorCodes ? errno : 0;
	int LastError	= AddictionalErrorCodes ? GetLastError() : 0;

	char String[PROGRAM_ERROR_STR_SIZE] = "";
	int Size = snprintf (String, PROGRAM_ERROR_STR_SIZE, 
						 "\nError!\n"
						 "====================================\n"
						 "%s: ", Reason1);
	if (Size == -1)
		Size = PROGRAM_ERROR_STR_SIZE;

	if (SReason2)
		Size += snprintf (&String[Size], PROGRAM_ERROR_STR_SIZE - Size, 
						 "%s\n", SReason2);
	else
		Size += snprintf (&String[Size], PROGRAM_ERROR_STR_SIZE - Size, 
						 "%d\n", IReason2);
	if (Size == -1)
		Size = PROGRAM_ERROR_STR_SIZE;

	if (Description)
	{
		Size += snprintf (&String[Size], PROGRAM_ERROR_STR_SIZE - Size, 
						  "Description: %s\n", Description);
		if (Size == -1)
			Size = PROGRAM_ERROR_STR_SIZE;
	}

	Size += snprintf (&String[Size], PROGRAM_ERROR_STR_SIZE - Size, 
					  "File: %s\n"
					  "Function: %s\n"
					  "Line: %d\n", File, Function, Line);
	if (Size == -1)
		Size = PROGRAM_ERROR_STR_SIZE;
	
	if (AddictionalErrorCodes)
	{
		Size += snprintf (&String[Size], PROGRAM_ERROR_STR_SIZE - Size, 
						  "errno: %d (%s)\n"
						  "GetLastError: %d\n", 
						  ErrNo, strerror (ErrNo), LastError);
		if (Size == -1)
			Size = PROGRAM_ERROR_STR_SIZE;
	}

	Size += snprintf (&String[Size], PROGRAM_ERROR_STR_SIZE - Size, 
					  "====================================");
	if (Size == -1)
		Size = PROGRAM_ERROR_STR_SIZE;

	SetWarningColor();
	LOG (String);
	SetNormalColor();
	
	__debugbreak();
	exit(1);
}