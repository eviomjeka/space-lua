﻿#ifndef CWINDOW
#define CWINDOW

#include <PlatformMisc.h>
#include "../../Utilities/SVector2D.h"
#include "../../Utilities/CArray.h"
#include <Keys.h>

class CWindow
{
public:

	CWindow();
	virtual ~CWindow();

	bool createWindow(bool fullscreen, SIVector2D windowSize, SIVector2D minWindowSize);
	void mainLoop();

	SIVector2D getWindowSize();
	void setCursorPosition(SIVector2D position);
	void showCursor(bool show);
	bool isFullScreen();
	void fullscreenMode(bool fullscreen);
	void destroyWindow();
	
	void setRGBA();
	void setMinimumColorSize(int r, int g, int b, int a);
	void setDoubleBuffer();
	void setMinimumDepthSize(int d);

	void requestMSAA(int minimumBuffers);

	virtual void proc(int time) = 0;
	virtual void keyDown(unsigned key) = 0;
	virtual void keyUp(unsigned key) = 0;
	virtual void leftButtonDown() = 0;
	virtual void leftButtonUp() = 0;
	virtual void rightButtonDown() = 0;
	virtual void rightButtonUp() = 0;
	virtual void mouseWheel(int delta) = 0;
	virtual void mouseMotion(SIVector2D position) = 0;
	virtual void resize() = 0;
	virtual void unfocus() = 0;
	virtual void destroy() = 0;

private:

	std::vector<int> requiredAttributes_;
	std::vector<int> desiredAttributes_;

	HWND  window_;
	HDC	  DC_;
	HGLRC RC_;

	bool hideCursor_;
	bool focused_;
	bool fullscreen_;
	bool destroy_;
	bool ignoreResize_;

	SIVector2D prevousCursorPosition_;
	SIVector2D windowPosition_;
	SIVector2D windowSize_;
	SIVector2D noFullscreenWindowPosition_;
	SIVector2D noFullscreenWindowSize_;
	SIVector2D minWindowSize_;
	SIVector2D windowBorderSize_;

	void initGLEW_(std::string windowClassName, PIXELFORMATDESCRIPTOR* pixelFormatDescriptor);

	static LRESULT CALLBACK wndProc_(HWND window, UINT message, WPARAM wParam, LPARAM lParam);

};

#endif
