﻿#include <sys/time.h>
#include "CWindow.h"
#include "../../Include.h"
#include "../../Utilities/CPngImage.h"

CWindow* __CWindow;

int CWindow::keyTable_[256] = {};

CWindow::CWindow() :
	display_(NULL),
	screen_(0),
	window_(0),
	context_(0),
	emptyCursor_(0),
	deleteWindowAtom_(0),
	position_(-1, -1),
	size_(-1, -1),
	fullscreen_(false),
	closed_(true),
	hideCursor_(false)
{}

CWindow::~CWindow()
{}

bool CWindow::createWindow(bool fullscreen, SIVector2D windowSize, SIVector2D minWindowSize)
{
	fullscreen_	= fullscreen;
	closed_		= false;
	hideCursor_	= false;

	size_		= windowSize;
	position_	= SIVector2D(0, 0);

	display_ = XOpenDisplay(NULL);
	ASSERT(display_);

	initKeyTable_();

	ASSERT(glXQueryExtension(display_, NULL, NULL));
	screen_ = DefaultScreen(display_);

	GLXFBConfig fbConfig;
	bool desired = getFBConfig_(fbConfig);

	XVisualInfo* visual = glXGetVisualFromFBConfig(display_, fbConfig);
	ASSERT(visual);

	XSetWindowAttributes winAttr;
	winAttr.colormap 			= XCreateColormap(display_,
									   			  RootWindow(display_, visual->screen),
									   			  visual->visual, AllocNone);
	winAttr.border_pixel		= 0;
	winAttr.background_pixel	= 0;
	winAttr.event_mask 			= KeyPressMask | KeyReleaseMask | ExposureMask | FocusChangeMask |
		StructureNotifyMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask;

	window_ = XCreateWindow (display_, RootWindow(display_, visual->screen),
							 position_.x, position_.y, size_.x, size_.y,
							 0, visual->depth, InputOutput, visual->visual,
							 CWBorderPixel | CWColormap | CWEventMask, &winAttr);

	ASSERT(window_);

	XStoreName(display_, window_, APPLICATION_NAME);

	context_ = glXCreateNewContext(display_, fbConfig, GLX_RGBA_TYPE, None, True);
	ASSERT(context_);
	glXMakeCurrent(display_, window_, context_);

	emptyCursor_ = getEmptyCursor_();
	deleteWindowAtom_ = XInternAtom(display_, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(display_, window_, &deleteWindowAtom_, 1);

	XMapWindow(display_, window_);
	fullscreenMode(fullscreen);

	GLenum errorCode = glewInit();
	if (errorCode != GLEW_OK)
		error((const char*) glewGetErrorString(errorCode));

	ASSERT(glewIsSupported("GL_VERSION_2_0"));
	glassert();
	//SetIcon();

	return desired;
}

void CWindow::mainLoop()
{
	timeval oldTime;
	gettimeofday(&oldTime, NULL);
	resize();
	
	glXMakeCurrent(display_, window_, context_);

	while (!closed_)
	{
		timeval newTime;
		gettimeofday(&newTime, NULL);
		int deltaTime = ((newTime.tv_sec  - oldTime.tv_sec) * 1000 +
						 (newTime.tv_usec - oldTime.tv_usec) / 1000);

		proc(deltaTime);
		oldTime = newTime;
		//XFlush(fisplay_);
		glXSwapBuffers(display_, window_);
		handleEvents_();
	}
}

//==============================================================================

SIVector2D CWindow::getWindowSize()
{
	return size_;
}

void CWindow::setCursorPosition(SIVector2D position)
{
	XWarpPointer(display_, window_, window_, position_.x, position_.y,
				 size_.x, size_.y, position.x, position.y);
}

void CWindow::showCursor(bool show)
{
	if (show && hideCursor_)
	{
		hideCursor_ = false;
		XUndefineCursor(display_, window_);
	}
	else if (!show && !hideCursor_)
	{
		hideCursor_ = true;
		XDefineCursor(display_, window_, emptyCursor_);
	}
}

bool CWindow::isFullScreen()
{
	return fullscreen_;
}

void CWindow::fullscreenMode(bool fullscreen)
{	
	if (fullscreen == fullscreen_)
		return;
	
	fullscreen_ = fullscreen;

	XWindowAttributes windowAttributes;
	XGetWindowAttributes(display_, window_, &windowAttributes);

	XEvent event = {};
	event.xclient.type = ClientMessage;
	event.xclient.message_type = XInternAtom(display_, "_NET_WM_STATE", false);
	event.xclient.display = display_;
	event.xclient.window = window_;
	event.xclient.format = 32;
	event.xclient.data.l[0] = 2;
	event.xclient.data.l[1] = XInternAtom(display_, "_NET_WM_STATE_FULLSCREEN", false);

	XSendEvent(display_, windowAttributes.root,
		false, SubstructureNotifyMask | SubstructureRedirectMask, &event);
}

void CWindow::destroyWindow()
{
	XEvent closeEvent;
	memset(&closeEvent, 0, sizeof (closeEvent));

	closeEvent.xclient.type			= ClientMessage;
	closeEvent.xclient.send_event	= True;
	//closeEvent.xclient.display		= display_;
	closeEvent.xclient.window		= window_;
	closeEvent.xclient.message_type	= XInternAtom(display_, "WM_PROTOCOLS", true);
	closeEvent.xclient.format		= 32;
	closeEvent.xclient.data.l[0]	= deleteWindowAtom_;
	//closeEvent.xclient.data.l[1] = currentTime;

	XSendEvent(display_, window_, False, NoEventMask, &closeEvent);
}

//==============================================================================

Cursor CWindow::getEmptyCursor_()
{
	char emptyCursorBits[32] = "";
	XColor dontCare;
	Cursor emptyCursor;
	Pixmap emptyCursorPixmap;

	memset(emptyCursorBits, 0, sizeof (emptyCursorBits));
	memset(&dontCare, 0, sizeof (dontCare));
	emptyCursorPixmap = XCreateBitmapFromData(display_,
		RootWindow(display_, screen_), emptyCursorBits, 16, 16);

	emptyCursor = XCreatePixmapCursor(display_, emptyCursorPixmap,
		emptyCursorPixmap,  &dontCare, &dontCare, 0, 0);
	XFreePixmap(display_, emptyCursorPixmap);

	return emptyCursor;
}

void CWindow::destroy_()
{
	destroy();
	glXMakeCurrent(display_, None, None);
	glXDestroyContext(display_, context_);
	XDestroyWindow(display_, window_);
	XCloseDisplay(display_);
	closed_ = true;
}

void CWindow::handleEvents_()
{
	XEvent event;
	while (!closed_ && XPending(display_))
	{
		XNextEvent(display_, &event);

		switch (event.type)
		{
			case ConfigureNotify:
				size_ = SIVector2D(event.xconfigure.width, event.xconfigure.height);
				resize();
			break;

			case ButtonPress:
				switch (event.xbutton.button)
				{
					case Button1:
						leftButtonDown();
					break;

					case Button3:
						rightButtonDown();
					break;

					default:
					break;
				}
			break;

			case ButtonRelease:
				switch (event.xbutton.button)
				{
					case Button1:
						leftButtonUp();
					break;

					case Button3:
						rightButtonUp();
					break;

					case Button4:
						mouseWheel(-1);
					break;

					case Button5:
						mouseWheel(1);
					break;

					default:
					break;
				}
			break;

			case KeyPress:
			{
				//wchar_t str[1];
				//Status returnStatus;
				int strLength = 0/*XwcLookupString
				(input_context_, &(xevent.xkey), Str, 10, NULL, &ReturnStatus)*/;
				if (!strLength)
				{
					int keyCode = event.xkey.keycode;
					ASSERT(keyCode >= 0 && keyCode < 256);
					keyDown(keyTable_[keyCode]);
				}
				else
				{
					//ASSERT(strLength == 1);
					//Character(*str);
				}
			}
			break;

			case KeyRelease:
			{
				int keyCode = event.xkey.keycode;
				ASSERT(keyCode >= 0 && keyCode < 256);
				keyUp(keyTable_[keyCode]);
			}
			break;

			case MotionNotify:
				 mouseMotion(SIVector2D(event.xmotion.x, event.xmotion.y));
			break;

			case FocusIn:
				break;

			case FocusOut:
				unfocus();
				break;

			case ClientMessage:
				if ((Atom) event.xclient.data.l[0] == deleteWindowAtom_)
					destroy_();
			break;
		}
	}
}

void CWindow::initKeyTable_()
{
#define DECL_KEY(APPNAME, XNAME) (int) APPNAME
	int keySyms[] =
		{
			#include "Keys.h"
		};
#undef DECL_KEY

	for (unsigned i = 0; i < sizeof (keySyms) / sizeof (int); i++)
	{
		int keyCode = XKeysymToKeycode(display_, keySyms[i]);

		if (keyCode)
			keyTable_[keyCode] = keySyms[i];
	}
}

void CWindow::setIcon(const char* pngIcon)
{
	//32bit an 64bit magic detected!!!
	byte* _data = NULL;
	SIVector2D iconSize = CPngImage::Instance.LoadFromFile(pngIcon, NULL, &_data);
	unsigned long* data = new unsigned long[iconSize.x * iconSize.y + 2];
	data[0] = iconSize.x;
	data[1] = iconSize.y;

	//shit just got serious
	for (int x = 0; x < iconSize.x; x++)
		for (int y = 0; y < iconSize.y; y++)
		{
			byte* pixel = (byte*) &data[(iconSize.y - y - 1) * iconSize.x + x + 2];

			int i = y * iconSize.x + x;
			pixel[0] = _data[i * 4 + 2];
			pixel[1] = _data[i * 4 + 1];
			pixel[2] = _data[i * 4 + 0];
			pixel[3] = _data[i * 4 + 3];
		}

    Atom _NET_WM_ICON	= XInternAtom(display_, "_NET_WM_ICON", False);
    Atom CARDINAL 		= XInternAtom(display_, "CARDINAL", False);

    XChangeProperty(display_, window_, _NET_WM_ICON, CARDINAL, 32, 
		PropModeReplace, (byte*) data, iconSize.x * iconSize.y + 2);

    CPngImage::Instance.DeleteData();
    delete[] data;
}

bool CWindow::getFBConfig_(GLXFBConfig& fbConfig)
{
	requiredAttributes_.push_back(None);
	desiredAttributes_.insert(desiredAttributes_.end(), requiredAttributes_.begin(), requiredAttributes_.end());

	int numFBConfigs = 0;
	GLXFBConfig* fbConfigs = glXChooseFBConfig(display_, screen_, &desiredAttributes_[0], &numFBConfigs);
	bool desired = (bool) fbConfigs;

	if (!fbConfigs)
	{
		WARNING("unable to get requested OpenGL context. Trying simpler one.");
		WARNING("it seems your machine does not support MSAA or whatever. Check your settings.");
		fbConfigs = glXChooseFBConfig(display_, screen_, &requiredAttributes_[0], &numFBConfigs);
	}

	ASSERT(fbConfigs);
	fbConfig = fbConfigs[0];

	XFree(fbConfigs);

	return desired;
}

void CWindow::setRGBA()
{
	requiredAttributes_.push_back(GLX_RENDER_TYPE);
	requiredAttributes_.push_back(GL_TRUE);
}

void CWindow::setMinimumColorSize(int r, int g, int b, int a)
{
	requiredAttributes_.push_back(GLX_RED_SIZE);
	requiredAttributes_.push_back(r);
	requiredAttributes_.push_back(GLX_GREEN_SIZE);
	requiredAttributes_.push_back(g);
	requiredAttributes_.push_back(GLX_BLUE_SIZE);
	requiredAttributes_.push_back(b);
	requiredAttributes_.push_back(GLX_ALPHA_SIZE);
	requiredAttributes_.push_back(a);
}

void CWindow::setDoubleBuffer()
{
	requiredAttributes_.push_back(GLX_DOUBLEBUFFER);
	requiredAttributes_.push_back(GL_TRUE);
}

void CWindow::setMinimumDepthSize(int d)
{
	requiredAttributes_.push_back(GLX_DEPTH_SIZE);
	requiredAttributes_.push_back(d);
}

void CWindow::requestMSAA(int MinimumBuffers)
{
	desiredAttributes_.push_back(GLX_SAMPLE_BUFFERS);
	desiredAttributes_.push_back(GL_TRUE);
	desiredAttributes_.push_back(GLX_SAMPLES);
	desiredAttributes_.push_back(MinimumBuffers);
}
