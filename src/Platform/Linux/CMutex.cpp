﻿#include "CMutex.h"
#include "../../Include.h"

CMutex::CMutex()
{
	pthread_mutex_init(&Mutex_, NULL);
}

void CMutex::Lock()
{
 	ASSERT(!pthread_mutex_lock(&Mutex_));
}

void CMutex::Unlock()
{
 	ASSERT(!pthread_mutex_unlock(&Mutex_));
}

bool CMutex::TryLock()
{
 	int ret = pthread_mutex_trylock(&Mutex_);
	if (!ret) return true;
	ASSERT(errno == EBUSY);
	return false;
}

CMutex::~CMutex()
{
 	ASSERT(!pthread_mutex_destroy(&Mutex_));
}
