#ifndef __CMUTEX__
#define __CMUTEX__

#include <pthread.h>

class CMutex
{
private:
	pthread_mutex_t Mutex_;

public:
	CMutex();
	~CMutex();

	void Lock();
	void Unlock();
	bool TryLock();

	friend class CCond;
};

#endif
