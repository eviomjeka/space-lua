﻿#include <dirent.h>
#include <sys/stat.h>
#include "CFileSystem.h"
#include "../../Include.h"

void CFileSystem::FileList(vector<CFileSystemElement>* List, const char* Path)
{
	dirent* Entry = NULL;
	DIR* Directory = opendir(Path);

	ASSERT(Directory);

	while ((Entry = readdir(Directory)))
	{
		if (strcmp(Entry->d_name, ".") && strcmp(Entry->d_name, ".."))
		{
			CFileSystemElement FileSystemElement = {};
			FileSystemElement.Name = Entry->d_name;
			FileSystemElement.Directory = Entry->d_type == DT_DIR;
			List->push_back(FileSystemElement);
		}
	}

	closedir(Directory);
}

bool CFileSystem::CreateDir(const char* Path)
{
	struct stat	Stat; // Type name conflicts with function

	char ReadyPath[MAX_PATH];
	ToGameFolderPath(Path, ReadyPath); // TODO: do it everywhere
	if (stat(ReadyPath, &Stat))
	{
		ASSERT(!mkdir(ReadyPath, 0777));
		return true;
	}
	else
	{
		ASSERT(S_ISDIR(Stat.st_mode));
		return false;
	}
}

bool CFileSystem::DeleteDir(const char* Path)
{
	dirent* Entry = NULL;
	char EntryPath[MAX_FILE_SIZE] = "";
	DIR* Directory = opendir(Path);

	if (!Directory)
	{
		ASSERT(errno == ENOENT);
		return false;
	}

	while ((Entry = readdir(Directory)))
	{
		if (strcmp(Entry->d_name, ".") && strcmp(Entry->d_name, ".."))
		{
			snprintf(EntryPath, MAX_FILE_SIZE, "%s/%s", Path, Entry->d_name);

			if (Entry->d_type == DT_DIR)
				ASSERT(DeleteDir(EntryPath))
			else
				ASSERT(DeleteFile(EntryPath))

		}
	}

	closedir(Directory);
	ASSERT(!remove(Path));

	return true;
}

bool CFileSystem::DeleteFile(const char* Path)
{
	return remove(Path) == 0;
}

char CFileSystem::RenameDir(const char* Src, const char* Dest)
{
	if (rename(Src, Dest))
	{
		if (errno == ENOENT)
			return FILESYSTEM_RENAMEDIR_NOTFOUND;
		else if (errno == EEXIST)
			return FILESYSTEM_RENAMEDIR_EXISTS;
		else
			error("Not same device or other");
	}

	return FILESYSTEM_RENAMEDIR_SUCCESS;
}

char* CFileSystem::ToGameFolderPath(const char* Path, char* GamePath)
{
	snprintf (GamePath, MAX_PATH, "files/%s", Path);
	return GamePath;
}
