#include "../../CApplication.h"

int main()
{
	CApplication* application = new CApplication;

	application->init();
	application->mainLoop();
	delete application;

	return 0;
}
