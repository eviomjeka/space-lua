#ifndef __ERROR__
#define __ERROR__

#include "../../Utilities/CLog.h"

#define PROGRAM_ERROR_STR_SIZE 4096

#define ASSERTS ASSERTS_NORMAL

#if ASSERTS == ASSERTS_ALL || ASSERTS == ASSERTS_NORMAL

#define ASSERT(OK)													\
{																	\
	if (!(OK))														\
	{																\
		SetWarningColor();											\
		LOG ("Error!\n====================================\n"		\
			 "Assertation failed: " #OK "\n"						\
			 "File: %s\n"											\
			 "Function: %s\n"										\
			 "Line: %d\n"											\
			 "errno: %d (%s)\n"										\
			 "====================================\n", 				\
			 __FILE__, __PRETTY_FUNCTION__, __LINE__,				\
			 errno, strerror (errno));								\
		SetNormalColor();											\
																	\
		asm("int $3");												\
		abort();													\
	}																\
}

#define glassert()													\
{																	\
	int OpenGLError = glGetError();									\
	if (OpenGLError)												\
	{																\
		SetWarningColor();											\
		LOG ("Error!\n====================================\n"		\
			 "OpenGL Error: %d\n"									\
			 "File: %s\n"											\
			 "Function: %s\n"										\
			 "Line: %d\n"											\
			 "====================================\n",				\
			 OpenGLError,  __FILE__,								\
			__PRETTY_FUNCTION__, __LINE__);							\
		SetNormalColor();											\
																	\
		asm("int $3");												\
		abort();													\
	}																\
}

#define error(Text, ...)											\
{																	\
	char __Str[PROGRAM_ERROR_STR_SIZE] = "";						\
	sprintf (__Str, Text, ##__VA_ARGS__);							\
																	\
	SetWarningColor();												\
	LOG ("Error!\n====================================\n"			\
		 "Error: %s\n"												\
		 "File: %s\n"												\
		 "Function: %s\n"											\
		 "Line: %d\n"												\
		 "errno: %d (%s)\n"											\
		 "====================================\n", 					\
		 __Str, __FILE__, __PRETTY_FUNCTION__, __LINE__, 			\
		 errno, strerror (errno));									\
	SetNormalColor();												\
																	\
	asm("int $3");													\
	abort();														\
}


#if ASSERTS == ASSERTS_ALL
#define __ASSERT(OK) ASSERT(OK)
#else
#define __ASSERT(OK) (OK)
#endif

#else

#define ASSERT(OK) (OK)
#define __ASSERT(OK) (OK)
#define glassert()
#define error(Text, ...)

#endif

#endif
