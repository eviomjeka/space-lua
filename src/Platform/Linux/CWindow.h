#ifndef __CWINDOW__
#define __CWINDOW__

#include <vector>
#include <X11/Xlib.h>
#include <GL/glew.h>
#include <GL/glx.h>
#include "../../Utilities/SVector2D.h"

#define DECL_KEY(APPNAME, XNAME) APPNAME = XNAME
enum Keys
{
	#include "Keys.h"
};
#undef DECL_KEY

class CWindow
{
public:
	CWindow();
	virtual ~CWindow();

	bool createWindow(bool fullscreen, SIVector2D windowSize, SIVector2D minWindowSize);
	void mainLoop();

	SIVector2D getWindowSize();
	void setCursorPosition(SIVector2D position);
	void showCursor(bool show);
	bool isFullScreen();
	void fullscreenMode(bool fullscreen);
	void destroyWindow();
	void setIcon(const char* pngIcon = "Icon.png");

	void setRGBA();
	void setMinimumColorSize(int r, int g, int b, int a);
	void setDoubleBuffer();
	void setMinimumDepthSize(int d);

	void requestMSAA(int minimumBuffers);

	virtual void proc(int Time) = 0;
	virtual void keyDown(unsigned key) = 0;
	virtual void keyUp(unsigned key) = 0;
	virtual void leftButtonDown() = 0;
	virtual void leftButtonUp() = 0;
	virtual void rightButtonDown() = 0;
	virtual void rightButtonUp() = 0;
	virtual void mouseWheel(int delta) = 0;
	virtual void mouseMotion(SIVector2D position) = 0;
	virtual void resize() = 0;
	virtual void unfocus() = 0;
	virtual void destroy() = 0;

private:
	std::vector<int>	requiredAttributes_;
	std::vector<int>	desiredAttributes_;

	Display*	display_;
	unsigned	screen_;
	Window	  	window_;
	GLXContext	context_;
	Cursor		emptyCursor_;
	Atom		deleteWindowAtom_;

	SIVector2D position_;
	SIVector2D size_;
	bool fullscreen_;
	bool closed_;
	bool hideCursor_;
	
	static int keyTable_[256];

	Cursor getEmptyCursor_();
	void destroy_();
	void handleEvents_();
	void initKeyTable_();
	bool getFBConfig_(GLXFBConfig& fbConfig);


};

#endif
