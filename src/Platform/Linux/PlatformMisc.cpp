﻿#include "PlatformMisc.h"
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>

struct __SThreadInit
{
	void* Param;
	TThreadFunction* Func;
	volatile bool Ready;
};

void* __ThreadFunction(void* Param)
{
	__SThreadInit* PInitializer = (__SThreadInit*) Param;
	__SThreadInit Initializer = *PInitializer;
	PInitializer->Ready = true;

	Initializer.Func (Initializer.Param);

	return 0;
}

void LaunchThread(TThreadFunction ThreadFunction, void* Param)
{
	__SThreadInit Initializer = {};
	Initializer.Param = Param;
	Initializer.Func  = ThreadFunction;
	Initializer.Ready = false;

	pthread_t Thread;
	pthread_create(&Thread, 0,  __ThreadFunction, (void*) &Initializer);
	
	while (!Initializer.Ready);
}

void PrintLog (const char* Data)
{
	std::cout << Data;
}

void SetWarningColor()
{
	//std::cout << "\\033[31";
}

void SetNormalColor()
{
	//std::cout << "\\033[39";
}

void Sleep(int MSec)
{
	usleep(MSec * 1000);
}

int TimeMS()
{
	timeval Time;
	gettimeofday(&Time, NULL);
	return Time.tv_sec  * 1000 + Time.tv_usec / 1000;
}