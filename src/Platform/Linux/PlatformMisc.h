#ifndef __PLATFORMMISC__
#define __PLATFORMMISC__

typedef void (TThreadFunction) (void*);

void LaunchThread (TThreadFunction ThreadFunction, void* Param);
void SetWarningColor();
void SetNormalColor();

void PrintLog (const char* Data);

void Sleep (int MSec);
int TimeMS();

#define MAX_PATH 1024

#endif
