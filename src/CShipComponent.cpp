#include "CShipComponent.h"
#include "CShip.h"
#include "Blocks/CBlockWeapon.h"
#include "Blocks/CBlockEngine.h"
#include "CShipController.h"
#include "CWorld.h"

#define CHECKBLOCK(dx, dy, dz)															\
	if (construction[currentPos.x + dx][currentPos.y + dy][currentPos.z + dz])			\
	{																					\
		construction[currentPos.x + dx][currentPos.y + dy][currentPos.z + dz] = false;	\
		st.push(currentPos + SIVector(dx, dy, dz));										\
	}

CShipComponent::CShipComponent(CShip* parent, int ID, string name, CFile& construction, const btTransform& transform, SFVector color) :
	CCollisionObject(eShipComponent),
	parent_(parent),
	ID_(ID),
	name_(name),
	color_(color),
	mainBlock_(-1),
	shapeUpdated_(false),
	shape_(NULL),
	motionState_(NULL),
	body_(NULL)
{
	readShape_(construction);
	createBody_(transform);
}

void CShipComponent::addConstraint_(const tConstraint& constraint)
{
	if (!constraint.child->blocks_[constraint.child->mainBlock_].exist() || 
		!constraint.parent->blocks_[constraint.parent->mainBlock_].exist())
		return;

	if (!constraint.child->doesExist() || !constraint.parent->doesExist())
		return;

	CBlock* childBlock = &constraint.child->blocks_[constraint.childBlock];
	CBlock* parentBlock = &constraint.parent->blocks_[constraint.parentBlock];

	if (!childBlock->exist() || !parentBlock->exist())
		return;

	btHingeConstraint* _constraint = new btHingeConstraint(*constraint.child->body_, *constraint.parent->body_,
		constraint.child->principalTransformInverse_ * childBlock->getPosition(),
		constraint.parent->principalTransformInverse_ * childBlock->getPosition(),
		childBlock->getOrientation(), childBlock->getOrientation());

	WORLD->world_->addConstraint(_constraint);
	
	constraint.child->body_->addConstraintRef(_constraint);
	constraint.parent->body_->addConstraintRef(_constraint);
}

void CShipComponent::readShape_(CFile& construction)
{
	while (!construction.EndOfFile())
	{
		char c = 0;
		construction.Scan("%c", &c);
		construction.ungetChar(c);
		if (c == '~')
			break;

		CBlock block = WORLD->blockManager_->createBlock(construction, color_);

		if (abs(block.getPosition().x) > MAX_SHIP_SIZE ||
			abs(block.getPosition().y) > MAX_SHIP_SIZE ||
			abs(block.getPosition().z) > MAX_SHIP_SIZE)
		{
			ASSERT(false);
		}

		if (block.getType() == BlockMain)
		{
			ASSERT(!getMainBlock());
			mainBlock_ = (int) blocks_.size();
		}

		if (block.getType() == BlockHinge)
		{
			ASSERT(mainBlock_ == -1);
			mainBlock_ = blocks_.size();

			CShipComponent* parent = parent_->getComponentByPosition(block.getPosition() + block.getOrientation());
			ASSERT(parent && parent != this);

			unsigned i;
			bool found = false;
			for (i = 0; i < parent->blocks_.size(); i++)
				if (parent->blocks_[i].getPosition() == block.getPosition() + block.getOrientation())
				{
					found = true;
					break;
				}
			ASSERT(found);

			tConstraint hinge = {parent, this, i, blocks_.size()};

			constraints_.push_back(hinge);
			parent->constraints_.push_back(hinge);
		}

		string& name = block.getName();

		if (!name.empty())
			parent_->getController()->registerBlock(ID_, blocks_.size(), name);

		blocks_.push_back(block);

		construction.Scan(" ");
	}

	for (unsigned i = 0; i < blocks_.size(); i++)
		if (blocks_[i].getType() == BlockEngine)
			WORLD->particleSystem_->addEngine(this, &blocks_[i]);

	ASSERT(getMainBlock());
}

CBlock* CShipComponent::getBlockByWorldPositionClosest_(const btVector3&  pointA, const btVector3& pointB)
{
	btScalar inf = 1.0e+2f;
	btScalar dist = +inf;
	btVector3 closestBlock;

	for (int i = 0; i < shape_->getNumChildShapes(); i++)
	{
		btCollisionShape* childShape = shape_->getChildShape(i);
		ASSERT(childShape == WORLD->boxShape_);

		btVector3 localA = principalTransform_ * body_->getWorldTransform().inverse() * pointA;
		btVector3 localB = principalTransform_ * body_->getWorldTransform().inverse() * pointB;
		btVector3 block = principalTransform_ * shape_->getChildTransform(i).getOrigin();
		btVector3 dA = localA - block;
		btVector3 dB = localB - block;
		btScalar d = max(abs(dA.getX()), max(abs(dA.getY()), max(abs(dA.getZ()),
			max(abs(dB.getX()), max(abs(dB.getY()), abs(dB.getZ()))))));

		if (d < dist)
		{
			dist = d;
			closestBlock = block;
		}
	}

	LOG("%lf, (%lf, %lf, %lf)", dist, closestBlock.getX(), closestBlock.getY(), closestBlock.getZ());

	if (dist < +inf)
		return getBlockByWorldPosition(toWorldPosition(closestBlock));

	return NULL;
}

void CShipComponent::hit(CBullet* bullet, const btVector3& pointA, const btVector3& pointB)
{
	if (!doesExist())
		return;

	CBlock* target = getBlockByWorldPositionClosest_(pointA, pointB);

	ASSERT(target);

	bullet->hit();

	if (!target->exist())
		return;

	target->hit(1); // TODO: set real damage

	if (!target->exist())
	{
		destroyingBlock_(target);
		shapeUpdated_ = true;
	}
}

void CShipComponent::destroyingBlock_(CBlock* block)
{
	WORLD->particleSystem_->addExplosion(this, block);

	for (auto constraint : constraints_)
		if (constraint.parent == this && &blocks_[constraint.parentBlock] == block && constraint.child->doesExist())
		{
			CBlock* childBlock = &constraint.child->blocks_[constraint.childBlock];

			if (childBlock->exist())
			{
				constraint.child->destroyingBlock_(childBlock);
				childBlock->die();
				constraint.child->shapeUpdated_ = true;
			}
		}
}

void CShipComponent::removeOrphanedBlocks_()
{
	bool construction[2 * MAX_SHIP_SIZE + 3][2 * MAX_SHIP_SIZE + 3][2 * MAX_SHIP_SIZE + 3] = { };

	for (unsigned int i = 0; i < blocks_.size(); ++i)
	{
		if (!blocks_[i].exist())
			continue;

		SIVector pos = blocks_[i].getPosition() + SIVector(MAX_SHIP_SIZE + 1);
		construction[pos.x][pos.y][pos.z] = true;
	}

	stack<SIVector> st;

	if (getMainBlock()->exist())
	{
		st.push(getMainBlock()->getPosition() + SIVector(MAX_SHIP_SIZE + 1));
		construction[st.top().x][st.top().y][st.top().z] = false;
	}

	while (!st.empty())
	{
		SIVector currentPos = st.top();
		st.pop();

		CHECKBLOCK(1, 0, 0)
		CHECKBLOCK(-1, 0, 0)
		CHECKBLOCK(0, 1, 0)
		CHECKBLOCK(0, -1, 0)
		CHECKBLOCK(0, 0, 1)
		CHECKBLOCK(0, 0, -1)
	}

	for (unsigned int i = 0; i < blocks_.size(); ++i)
	{
		SIVector pos = (&blocks_[i])->getPosition() + SIVector(MAX_SHIP_SIZE + 1);

		if (construction[pos.x][pos.y][pos.z])
		{
			ASSERT(blocks_[i].exist());
			destroyingBlock_(&blocks_[i]);


			blocks_[i].die();
		}
	}
}

void CShipComponent::prepareForPhysicsStep()
{
	if (!doesExist())
		return;

	if (shapeUpdated_)
	{
		shapeUpdated_ = false;
		removeOrphanedBlocks_();

		if (!getMainBlock()->exist())
		{
			die();
			return;
		}

		rebuildShape_();
	}


	if (body_->getLinearVelocity().length2() > MAX_SHIP_VELOCITY * MAX_SHIP_VELOCITY)
		body_->setLinearVelocity(body_->getLinearVelocity().normalized() * MAX_SHIP_VELOCITY);

	if (body_->getCenterOfMassPosition().length2() >= 0.9f * DESPAWN_DISTANCE * DESPAWN_DISTANCE)
		body_->applyCentralForce(-body_->getCenterOfMassPosition() * GRAVITY_CONSTANT);
}

void CShipComponent::removeConstraints_()
{
	while (body_->getNumConstraintRefs())
	{
		btTypedConstraint* constraint = body_->getConstraintRef(0);
		WORLD->world_->removeConstraint(constraint);
		constraint->getRigidBodyA().updateInertiaTensor();
		constraint->getRigidBodyB().updateInertiaTensor();
		constraint->getRigidBodyA().removeConstraintRef(constraint);
		constraint->getRigidBodyB().removeConstraintRef(constraint);
		delete constraint;
	}
}

void CShipComponent::rebuildShape_()
{
	removeConstraints_();

	WORLD->world_->removeRigidBody(body_);
	delete shape_;

	btScalar mass;
	btVector3 inertia;
	buildShape_(mass, inertia);
	body_->setCollisionShape(shape_);
	body_->setMassProps(mass, inertia);
	body_->updateInertiaTensor();

	WORLD->world_->addRigidBody(body_);

	for (auto constraint : constraints_)
		addConstraint_(constraint);
}

void CShipComponent::createBody_(const btTransform& transform)
{
	/*XXX: updateChildTransform is too slow, so we recreate the shape*/
	btScalar mass;
	btVector3 inertia;
	buildShape_(mass, inertia);

	motionState_ = new btDefaultMotionState(transform * principalTransform_);

	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState_, shape_, inertia);
	body_ = new btRigidBody(rbInfo);

	body_->setUserPointer(this);

	WORLD->world_->addRigidBody(body_);

	for (auto constraint : constraints_)
		addConstraint_(constraint);
}

void CShipComponent::buildShape_(btScalar& mass, btVector3& inertia)
{
	mass = 0.0f;
	vector<btScalar> blockMasses;

	btCompoundShape* tempShape = new btCompoundShape();
	for (unsigned i = 0; i < blocks_.size(); i++)
		/*TODO: Fix hinge*/
		if (blocks_[i].exist() && blocks_[i].getBase()->getType() != BlockHinge)
		{
			float blockMass = blocks_[i].getBase()->getMass();
			blockMasses.push_back(blockMass);
			mass += blockMass;

			btTransform blockTransform(btQuaternion::getIdentity(), blocks_[i].getPosition());
			tempShape->addChildShape(blockTransform, WORLD->boxShape_);
		}

	tempShape->calculatePrincipalAxisTransform(blockMasses.data(), principalTransform_, inertia);
	principalTransform_.setIdentity();
	principalTransformInverse_ = principalTransform_.inverse();

	shape_ = new btCompoundShape();

	for (int i = 0; i < tempShape->getNumChildShapes(); i++)
		shape_->addChildShape(principalTransformInverse_ * tempShape->getChildTransform(i), tempShape->getChildShape(i));

	delete tempShape;
}


void CShipComponent::draw()
{
	if (!doesExist())
		return;

	btTransform shipTransform = body_->getWorldTransform() * principalTransformInverse_;
	for (unsigned i = 0; i < blocks_.size(); i++)
	{
		if (!blocks_[i].exist())
			continue;

		glPushMatrix();
		btTransform blockTransform = shipTransform * btTransform(btMatrix3x3::getIdentity(), blocks_[i].getPosition());
		btScalar glMatrix[16];
		blockTransform.getOpenGLMatrix(glMatrix);
		glMultMatrixf(glMatrix);
		blocks_[i].draw(0.0f);
		glPopMatrix();
	}
}

CBlock* CShipComponent::getBlock(unsigned ID)
{
	if (ID >= blocks_.size())
		return NULL;
	
	return &blocks_[ID];
}

void CShipComponent::die()
{
	ASSERT(doesExist());

	removeConstraints_();
	WORLD->world_->removeRigidBody(body_);
	delete body_;
	body_ = NULL;
	delete motionState_;
	motionState_ = NULL;
	delete shape_;
	shape_ = NULL;

	WORLD->particleSystem_->removeShipEngines(this);
}

CShipComponent::~CShipComponent()
{
	if (doesExist())
		die();
}

CBlock* CShipComponent::getMainBlock()
{
	if (mainBlock_ == -1)
		return NULL;

	return &blocks_[mainBlock_];
}

CBlock* CShipComponent::getBlockByLocalPosition(const SIVector& position)
{
	for (unsigned i = 0; i < blocks_.size(); i++)
		if (blocks_[i].getPosition() == position)
			return &blocks_[i];

	return NULL;
}

CBlock* CShipComponent::getBlockByWorldPosition(const btVector3& position)
{
	btVector3 localPosition = principalTransform_ * body_->getCenterOfMassTransform().inverse() * position;
	SIVector blockPosition(int(round(localPosition.getX())),
						   int(round(localPosition.getY())),
						   int(round(localPosition.getZ())));

	for (unsigned i = 0; i < blocks_.size(); i++)
		if (blocks_[i].getPosition() == blockPosition)
			return &blocks_[i];

	return NULL;
}

void CShipComponent::dumpState(lua_State* luaState)
{
	lua_newtable(luaState);

	LUA_ADD_BT_VECTOR(luaState, body_->getCenterOfMassPosition(), "mass_center_position")
	LUA_ADD_BT_VECTOR(luaState, toWorldPosition(), "position")
	LUA_ADD_BT_VECTOR(luaState, body_->getLinearVelocity(), "velocity")
	LUA_ADD_BT_VECTOR(luaState, body_->getAngularVelocity(), "angular_velocity")
	LUA_ADD_MATRIX(luaState, 3, (body_->getWorldTransform() *  principalTransformInverse_).getBasis(), "basis");

	lua_setfield(luaState, -2, name_.c_str());
}

bool CShipComponent::doesExist()
{
	return body_ != NULL;
}

CShip* CShipComponent::getParent()
{
	return parent_;
}

string CShipComponent::getName()
{
	return name_;
}

btVector3 CShipComponent::toWorldDirection(const btVector3& direction)
{
	return (body_->getWorldTransform() * principalTransformInverse_).getBasis() * direction;
}

btVector3 CShipComponent::toWorldPosition(const btVector3& localPosition)
{
	return body_->getWorldTransform() * principalTransformInverse_ * localPosition;
}

btVector3 CShipComponent::toWorldPositionRelative(const btVector3& localPosition)
{
	return toWorldPosition(localPosition) - body_->getCenterOfMassPosition();
}

btVector3 CShipComponent::toWorldVelocity(const btVector3& localPosition, const btVector3& localVelocity)
{
	return toWorldDirection(localVelocity) + body_->getVelocityInLocalPoint(toWorldPositionRelative(localPosition));
}

void CShipComponent::applyForceLocal(const btVector3& force, const btVector3& position)
{
	body_->applyForce(toWorldDirection(force), toWorldPositionRelative(position));
}
