#ifndef __CCOLLISIONOBJECT__
#define __CCOLLISIONOBJECT__

class CCollisionObject
{
public:
	enum tType
	{
		eBullet,
		eShipComponent
	};

private:
	tType Type_;
	
public:
	CCollisionObject(tType Type);
	tType getType();
		
};

#endif
