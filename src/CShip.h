#ifndef __CSHIP__
#define __CSHIP__

#include <cstdio>
#include <vector>
#include <stack>
#include <cstdlib>
#include <map>

#include "Utilities/SVector.h"
#include "Include.h"
#include "CShipComponent.h"
#include "CShipController.h"

using std::vector;
using std::stack;
using std::map;

#define INSTRUCTION_LIMIT 1000

#define GUN_COOLDOWN_MS 500

#define SHIP_LIVES 5

#define SHIP_SIZE 0.5f

#define HIT_ANIMATION_MS 500

#define MAX_SHIP_VELOCITY 10.0f
#define MAX_SHIP_ACCELERATION 0.000005f
#define MAX_SHIP_SIZE 50

#define MAX_POSITION 90

#define DEFAULT_VISION 100.0f

class CShip
{
private:
	CShipController			shipController_;
	SFVector				color_;
	int						team_;
	int						mainComponent_;

	vector<CShipComponent*>	components_;

	void initComponents_(const char* contruction, const btTransform& transform);

	CBlock* getBlockByWorldPosition_(btVector3& position);

public:
	CShip(const char* script, const char* construction, SFVector color, SFVector position, int team);
	~CShip();

	CShip(const CShip&);
	CShip& operator =(CShip&);

	void doStep(float dt);
	void draw();
	bool exist();
	void dumpState(lua_State* luaState);
	void prepareForPhysicsStep();
	void die();
	CBlock* getMainBlock();
	CShipComponent* getComponent(unsigned ID);
	CShipController* getController();
	CShipComponent* getComponentByPosition(SIVector position);
	int getTeam();


	friend class CBlockWeapon; // XXX: fucking shit;
	friend class CBlockEngine; // XXX: fucking shit
	friend class CApplication; // TODO: remove
	friend class CParticleSystem;

};

#endif
