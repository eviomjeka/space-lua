#ifndef __CAPPLICATION__
#define __CAPPLICATION__

#include "Include.h"
#include "CShip.h"
#include "CBullet.h"
#include "CBlockManager.h"
#include "CBlock.h"
#include "Utilities/CFont.h"
#include "CParticleSystem.h"

class CWorld;

#define APPLICATION	CApplication::instance
#define WORLD		APPLICATION->getWorld()
#define FONT		APPLICATION->getFont()

#define NUM_MOVE_DIRECTIONS 6
enum EMoveDirection
{
	MOVE_DIRECTION_LOW_X,
	MOVE_DIRECTION_HIGH_X,
	MOVE_DIRECTION_LOW_Z,
	MOVE_DIRECTION_HIGH_Z,
	MOVE_DIRECTION_LOW_Y,
	MOVE_DIRECTION_HIGH_Y
};

class CApplication : public CWindow
{
public:

	static const int WINDOW_WIDTH;
	static const int WINDOW_HEIGHT;
	static const int MIN_WINDOW_WIDTH;
	static const int MIN_WINDOW_HEIGHT;

	CApplication();
	virtual ~CApplication();
	static CApplication* instance;

	virtual void init();
	virtual void proc(int time);
	virtual void keyDown(unsigned key);
	virtual void keyUp(unsigned key);
	virtual void leftButtonDown();
	virtual void leftButtonUp();
	virtual void rightButtonDown();
	virtual void rightButtonUp();
	virtual void mouseWheel(int delta);
	virtual void mouseMotion(SIVector2D position);
	virtual void resize();
	virtual void unfocus();
	virtual void destroy();

	CWorld* getWorld();
	CFont* getFont();

private:

	void setOpenGLParameters_();
	void printOpenGLInformation_();

	void perspective_(float fovy, float aspect, float zNear, float zFar);

	void initLog_();
	void eventDown_(unsigned key);
	void eventUp_(unsigned key);

	CFont font_; // TODO: pointer
	CWorld* world_;

};

#endif
