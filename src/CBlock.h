#ifndef CBLOCK
#define CBLOCK

#include "Utilities/SVector.h"
#include <vector>
#include <string>
#include "EBlockType.h"

class CBlockBase;
class CBlockManager;

class CBlockWeapon;

using std::vector;
using std::string;

class CBlock
{
public:

    ~CBlock();

    void hit(int damage);

    CBlockBase* getBase();
    EBlockType getType();
	SIVector getPosition();
	SIVector getOrientation();
	int getLives();
	string& getName();
	bool exist();
	void die();
	void draw(float factor);

	float getPower();

private:

    CBlock();

	CBlockBase* base_;
    int lives_;
	string name_;

    SIVector position_;
    SIVector orientation_;
    SFVector color_;

	int lastShootTime_;
	float power_;

	friend class CBlockManager;
	friend class CBlockBase;

	friend class CBlockWeapon; // XXX: fuck this
	friend class CBlockEngine; // XXX: fuck this

};

#endif // CBLOCK
