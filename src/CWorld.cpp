#include "CWorld.h"

bool _ContactProcessedCallback(btManifoldPoint& cp, void* body0, void* body1)
{
	CCollisionObject* p1 = (CCollisionObject*)((btRigidBody*)body0)->getUserPointer();
	CCollisionObject* p2 = (CCollisionObject*)((btRigidBody*)body1)->getUserPointer();
	ASSERT(p1 && p2);

	if (p1->getType() == CCollisionObject::eShipComponent && p2->getType() == CCollisionObject::eShipComponent)
		return false;

	if (p1->getType() == CCollisionObject::eBullet && p2->getType() == CCollisionObject::eBullet)
	{
		((CBullet*)p1)->hit();
		((CBullet*)p2)->hit();
		return false;
	}

	btVector3 pointA = cp.getPositionWorldOnA();
	btVector3 pointB = cp.getPositionWorldOnB();

	if (p1->getType() == CCollisionObject::eShipComponent)
		((CShipComponent*)p1)->hit((CBullet*)p2, pointA, pointB);
	else
		((CShipComponent*)p2)->hit((CBullet*)p1, pointB, pointA);

	return false;
}

void internalBulletTickCallbackPre(btDynamicsWorld *world, btScalar timeStep)
{
	for (int i = 0; i < NUM_SHIPS; i++)
		WORLD->ship_[i]->prepareForPhysicsStep();

	for (int i = 0; i < MAX_BULLETS; i++)
		WORLD->bullets_[i].checkExistance();

	for (int i = 0; i < NUM_SHIPS; i++)
		WORLD->ship_[i]->doStep(timeStep);
}

void CWorld::init()
{
	gContactProcessedCallback = _ContactProcessedCallback;

	glEnable(GL_DEPTH_TEST);
	glDepthFunc (GL_LESS);
	Orientation_ = SFVector2D(0);
	Position_ = SFVector(0);
	Velocity_ = SFVector(0);
	particleSystem_ = new CParticleSystem;
	particleSystem_->Init();

	SFVector colors[] = {SFVector(1.0f, 0.0f, 0.0f), SFVector(0.0f, 1.0f, 0.0f), SFVector(0.0f, 0.0f, 1.0f)};
	blockManager_ = new CBlockManager("constructions/database.txt");
	ASSERT(NUM_TEAMS <= 3);

	broadphase_ = new btDbvtBroadphase();
	collisionConfiguration_ = new btDefaultCollisionConfiguration();
	dispatcher_ = new btCollisionDispatcher(collisionConfiguration_);
	solver_ = new btSequentialImpulseConstraintSolver;
	world_ = new btDiscreteDynamicsWorld(dispatcher_, broadphase_, solver_, collisionConfiguration_);
	world_->setInternalTickCallback(internalBulletTickCallbackPre, NULL, true);
	world_->setGravity(btVector3(0, 0, 0));
	boxShape_ = new btBoxShape(btVector3(0.5f, 0.5f, 0.5f));
	sphereShape_ = new btSphereShape(BULLET_RADIUS);

	for (int i = 0; i < NUM_SHIPS; i++)
	{
		int team = 1 + i % NUM_TEAMS;

		char scriptName[MAX_PATH] = "";
		char readyScriptName[MAX_PATH] = "";
		char constructionName[MAX_PATH] = "";
		sprintf(scriptName, "scripts/bot%d.lua", 1);
		sprintf(constructionName, "constructions/bot%d.txt", 1);
		CFileSystem::ToGameFolderPath(scriptName, readyScriptName);

		#define RAND_COORD() ((rand() - RAND_MAX / 2.0f) * 100.0f / RAND_MAX)

		ship_[i] = new CShip(readyScriptName, constructionName, colors[i % NUM_TEAMS], SFVector(RAND_COORD(), RAND_COORD(), RAND_COORD()), team);

		#undef RAND_COORD
	}

	glClearColor(0.0f, 0.0f, 0.04f, 1.0f);
	glPointSize(1.0f);
	int j = 0;
	for (int i = 0; i < NUM_STARS; i++, j++)
	{
		SIVector RandVector (rand() % 2001 - 1000, rand() % 2001 - 1000, rand() % 2001 - 1000);
		float SqRandVectorSize = (float) RandVector.SqLen();
		if (SqRandVectorSize > 1000.0f * 1000.0f)
		{
			i--;
			continue;
		}
		float RandVectorSize = sqrt (SqRandVectorSize);
		SFVector StarPosition (1680.0f * RandVector.x / RandVectorSize,
							   1680.0f * RandVector.y / RandVectorSize,
							   1680.0f * RandVector.z / RandVectorSize);
		stars_[i] = StarPosition;
	}

	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	GLfloat material_diffuse[] = {1.0, 1.0, 1.0, 1.0};
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_diffuse);

	GLfloat color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat shininess = 100.0f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &shininess);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color);

	glEnable(GL_LIGHT0);
	GLfloat light0_position[] = { 1000.0f, 1500.0f, 0.0f, 1.0f };
	GLfloat light0_diffuse[] = { 1.0f, 1.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);

	CTexture t;
	CPngImage::Instance.LoadFromFile("blocks.png", &t);
	blocks_ = t.ID();
	glBindTexture(GL_TEXTURE_2D, blocks_);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	for (int i = 0; i < MAX_BULLETS; i++)
		bullets_[i].setN(i);
}

void CWorld::destroy()
{
	for (int i = 0; i < NUM_SHIPS; i++)
		delete ship_[i];
		
	for (int i = 0; i < MAX_BULLETS; i++)
		if (bullets_[i].exist())
			bullets_[i].die();
	
	delete boxShape_;
	delete sphereShape_;
    delete world_;
    delete solver_;
    delete collisionConfiguration_;
    delete dispatcher_;
    delete broadphase_;
    delete blockManager_;
	
	glDeleteTextures(1, &blocks_);

	particleSystem_->Destroy();
	delete particleSystem_;
}

void CWorld::DrawSkydome_()
{
	glDisable(GL_LIGHTING);
	glBegin(GL_POINTS);

	glColor3f(1.0f, 1.0f, 1.0f);
	for (int i = 0; i < NUM_STARS; i++)
	{
		glVertex3f(stars_[i].x, stars_[i].y, stars_[i].z);
	}

	glEnd();
	glEnable(GL_LIGHTING);
}

void CWorld::proc(int Time)
{
	glPushMatrix();
	glLoadIdentity();
	glRotatef(Orientation_.x, 1.0f, 0.0f, 0.0f);
	glRotatef(Orientation_.y, 0.0f, 1.0f, 0.0f);

	DrawSkydome_();

	glTranslatef(-Position_.x, -Position_.y, -Position_.z);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, blocks_);
	for (int i = 0; i < NUM_SHIPS; i++)
	{
		ship_[i]->draw();
	}
	glDisable(GL_TEXTURE_2D);

	for (int i = 0; i < MAX_BULLETS; i++)
	{
		if (bullets_[i].exist())
			bullets_[i].draw();
	}

	particleSystem_->Proc(Time);

	glPopMatrix();

	float Alpha = Orientation_.y * PI / 180.0f;
	float dX = (cos(Alpha) * Velocity_.x + sin(Alpha) * Velocity_.z) * (Time / 1000.0f);
	float dY = Velocity_.y * (Time / 1000.0f);
	float dZ = (sin(Alpha) * Velocity_.x - cos(Alpha) * Velocity_.z) * (Time / 1000.0f);

	Position_ += SFVector(dX, dY, dZ);

	world_->stepSimulation(Time / 1000.0f, 10);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, 1.0f, 0.0f, APPLICATION->getWindowSize().y / (float) APPLICATION->getWindowSize().x, -1.0f, 1.0f);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glTranslatef(0.02f, 0.02f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	FONT->Render(APPLICATION_NAME, 0.02f);
	glTranslatef(0.0f, 0.03f, 0.0f);
	glColor3f(0.3f, 0.3f, 0.3f);
	FONT->Render("Press 'C' for constructor mode", 0.015f);

	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glMatrixMode(GL_MODELVIEW);
}

CBullet& CWorld::getFreeBullet()
{
	int i = 0;
	bool found = false;
	for (i = 0; i < MAX_BULLETS; i++)
	{
		if (!bullets_[i].exist())
		{
			found = true;
			break;
		}
	}

	ASSERT(found);
	return bullets_[i];
}

void CWorld::moveDirection(int Direction, bool Move)
{
	float Speed = 30.0f;
	if (!Move)
		Speed = 0.0f;

	switch (Direction)
	{
		case MOVE_DIRECTION_LOW_X:  Velocity_.x = -Speed;	break;
		case MOVE_DIRECTION_HIGH_X: Velocity_.x = Speed;	break;
		case MOVE_DIRECTION_LOW_Z:  Velocity_.z = -Speed;	break;
		case MOVE_DIRECTION_HIGH_Z: Velocity_.z = Speed;	break;
		case MOVE_DIRECTION_LOW_Y:  Velocity_.y = -Speed;	break;
		case MOVE_DIRECTION_HIGH_Y: Velocity_.y = Speed;	break;
		default: error ("Unknown move direction");			break;
	}
}

void CWorld::rotate(SFVector2D delta)
{
	Orientation_ += delta;
	if (Orientation_.x >  90.0f) Orientation_.x =  90.0f;
	if (Orientation_.x < -90.0f) Orientation_.x = -90.0f;

	if (Orientation_.y <= 0.0f || Orientation_.y >= 360.0f)
		Orientation_.y = Orientation_.y - floor (Orientation_.y / 360.0f) * 360.0f;
}
