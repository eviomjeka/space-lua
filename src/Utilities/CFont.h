﻿#ifndef CFONT
#define CFONT

#include "SVector.h"
#include "CPngImage.h"

class CFont
{
private:

	float						SymbolSize_[256];
	bool						Initialized_;
	CTexture					Texture_;

public:

	CFont();
	void Init();
	void Destroy();
	~CFont();

	void Render(const char* String, float Size, int Start = 0, int End = -1);
	float GetLength(const char* String, float Size, int Start = 0, int End = -1);

};

#endif
