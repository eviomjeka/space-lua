﻿#include "CRandom.h"
#include "../Include.h"
#include "Misc.h"

int CRandom::Mix (int a, int b, int c)
{
	a=a-b;	a=a-c;	a=a^(c >> 13);
	b=b-c;	b=b-a;	b=b^(a << 8); 
	c=c-a;	c=c-b;	c=c^(b >> 13);
	a=a-b;	a=a-c;	a=a^(c >> 12);
	b=b-c;	b=b-a;	b=b^(a << 16);
	c=c-a;	c=c-b;	c=c^(b >> 5);
	a=a-b;	a=a-c;	a=a^(c >> 3);
	b=b-c;	b=b-a;	b=b^(a << 10);
	c=c-a;	c=c-b;	c=c^(b >> 15);

	return c;
}

int CRandom::RandomIntFromRand()
{
	unsigned a = (unsigned) rand();
	a <<= 15;
	a += (unsigned) rand();
	a <<= 2;
	a += (unsigned) rand() % 4;
	return (int) a;
}

void CRandom::Srand()
{
	srand ((unsigned) time (NULL));
	rand();
}

CRandom::CRandom() :
	Seed_ (0),
	ID_ (0)
{}

CRandom::CRandom (int Seed) :
	Seed_ (Seed),
	ID_ (0)
{}

void CRandom::SetSeed (int Seed)
{
	ID_ = 0;
	Seed_ = Seed;
}

void CRandom::SetRandomSeed()
{
	SetSeed (RandomIntFromRand());
}

int CRandom::operator () ()
{
	return (NextInt() & 0x7FFFFFFF);
}

int CRandom::operator () (int Min, int Max)
{
	return NextNumber (Min, Max);
}

int CRandom::NextInt()
{
	ID_++;
	return Mix (Seed_, ID_, 0);
}

int CRandom::NextNumber (int Min, int Max)
{
	return Min + Mod (NextInt(), Max - Min);
}

int CRandom::Seed()
{
	return Seed_;
}

CRandom CRandom::RandomFromCoordinates (SIVector2D Coordinates)
{
	return CRandom (Mix (Seed_, Coordinates.x, Coordinates.y));
}
