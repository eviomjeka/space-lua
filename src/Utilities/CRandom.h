#ifndef __CRANDOM__
#define __CRANDOM__

#include <cstdlib>
#include "SVector2D.h"

class CRandom
{
public:

	static int Mix (int a, int b, int c);
	static int RandomIntFromRand();
	static void Srand();

	CRandom();
	CRandom (int Seed);

	void SetSeed (int Seed);
	void SetRandomSeed();

	int operator () ();
	int operator () (int Min, int Max);

	int NextInt();
	int NextNumber (int Min, int Max);

	int Seed();
	CRandom RandomFromCoordinates (SIVector2D Coordinates);

private:

	int Seed_;
	int ID_;
	
};

#endif
