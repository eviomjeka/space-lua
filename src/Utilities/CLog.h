#ifndef __CLOG__
#define __CLOG__

#include <fstream>

#define LOG		CLog::Instance.Log
#define __LOG	CLog::Instance.LogWithoutTime
#define WARNING	CLog::Instance.Warning

#define LOG_STR_SIZE (1 << 15)

void PrintLog (const char* Data);
void SetWarningColor();
void SetNormalColor();

class CLog
{
public:

	static CLog Instance;

	void Init();
	void Destroy();

	void Log (const char* Format, ...);
	void LogWithoutTime (const char* Format, ...);
	void Warning (const char* Format, ...);

private:
	
	void Log_ (const char* Buffer);

	std::ofstream File_; // TODO: use CFile

};

#endif
