﻿#include "CLog.h"
#include "../Include.h"

CLog CLog::Instance;

void CLog::Init()
{
	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);

	char str[MAX_PATH] = "";
	char readyPath[MAX_PATH] = "";
	strftime (str, 255, "logs/log [%d-%m-%y %H.%M.%S].txt", timeinfo);
	CFileSystem::ToGameFolderPath(str, readyPath);
	File_.open(readyPath);

	SetNormalColor();
}

void CLog::Destroy()
{
	File_.close();
}

void CLog::Log_ (const char* Buffer)
{
	PrintLog (Buffer);

	if (!File_)
	{
		SetWarningColor();
		PrintLog ("WARNING: WRITE INFO TO LOG: ERROR! FILE NOT OPENED!\n");
		SetNormalColor();
	}
	else
	{
		File_ << Buffer;
		File_.flush();
	}
}

void CLog::Log (const char* Format, ...)
{
	va_list vl;
	va_start (vl, Format);

	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);
	
	char Buffer[LOG_STR_SIZE] = "";
	size_t Size = strftime (Buffer, LOG_STR_SIZE - 1, "[%H:%M:%S]: ", timeinfo);
	Size += vsnprintf (&Buffer[Size], LOG_STR_SIZE - Size - 1, Format, vl);
	
	if (Size + 1 >= LOG_STR_SIZE)
		Size = LOG_STR_SIZE - 2;
	Buffer[Size] = '\n';
	Buffer[Size + 1] = 0;

	va_end (vl);

	Log_ (Buffer);
}

void CLog::LogWithoutTime (const char* Format, ...)
{
	va_list vl;
	va_start (vl, Format);

	char Buffer[LOG_STR_SIZE] = "";
	vsnprintf (Buffer, LOG_STR_SIZE - 1, Format, vl);

	va_end (vl);

	Log_ (Buffer);
}

void CLog::Warning (const char* Format, ...)
{
	va_list vl;
	va_start (vl, Format);

	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);
	
	char Buffer[LOG_STR_SIZE] = "";
	size_t Size = strftime (Buffer, LOG_STR_SIZE - 1, "[%H:%M:%S]: ", timeinfo);
	strncpy (&Buffer[Size], "WARNING: ", LOG_STR_SIZE - Size - 1);
	Size = strlen (Buffer);
	Size += vsnprintf (&Buffer[Size], LOG_STR_SIZE - Size - 1, Format, vl);
	
	if (Size + 1 >= LOG_STR_SIZE)
		Size = LOG_STR_SIZE - 2;
	Buffer[Size] = '\n';
	Buffer[Size + 1] = 0;

	va_end (vl);

	SetWarningColor();
	Log_ (Buffer);
	SetNormalColor();
}
