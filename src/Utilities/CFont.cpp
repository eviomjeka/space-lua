﻿#include "CFont.h"
#include "../Include.h"

CFont::CFont() :
	Initialized_(false)
{}

void CFont::Init()
{
	ASSERT (!Initialized_);
	
	byte* Data = NULL;
	SIVector2D Size = CPngImage::Instance.LoadFromFile ("font_%d.png", &Texture_, &Data, 3);
	
	for (int i = 0; i < 256; i++)
	{
		SIVector2D TextureCoord (i % 16, i / 16);
		TextureCoord *= Size.x / 16;
		bool CharFounded = false;
		for (int j = Size.x / 16 * 15 / 16 - 1; j >= 0; j--)
		{
			for (int k = 0; k < (int) Size.x / 16; k++)
			{
				int x = TextureCoord.x + j;
				int y = TextureCoord.y + k;
				int Pos = 4 * (x + Size.x * (Size.y - y - 1));
				if (Data[Pos] >= 200)
				{
					CharFounded = true;
					break;
				}
			}
			if (CharFounded)
			{
				SymbolSize_[i] = (float) j + 3.0f;
				break;
			}
		}
		if (!CharFounded)
		{
			if (i == (unsigned char) ' ')
				SymbolSize_[i] = (Size.x / 16.0f - 1.0f) / 4.0f;
			else
				SymbolSize_[i] = 3.0f;
		}
		SymbolSize_[i] /= Size.x / 16.0f - 1.0f;
	}
	CPngImage::Instance.DeleteData();

	Initialized_ = true;
}

void CFont::Destroy()
{
	Texture_.Destroy();

	Initialized_ = false;
}

CFont::~CFont()
{
	ASSERT (!Initialized_);
}

#define GL_VERTEX(v, t)					\
{										\
	SFVector2D temp2 = t;				\
	glTexCoord2f(temp2.x, temp2.y);		\
	SFVector temp = v;					\
	glVertex3f(temp.x, temp.y, temp.z);	\
}

void CFont::Render(const char* String, float Size, int Start, int End)
{
	ASSERT (Initialized_);
	ASSERT (Start >= 0 && (End >= 0 || End == -1));
	ASSERT (End > Start || End == -1);

	const unsigned char* UString = (const unsigned char*) String;
	Size *= 1.3f;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Texture_.ID());
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.01f);
	glBegin(GL_TRIANGLES);

	SFVector TranslateVector;

	for (int i = Start; (End != -1 && i < End) || (End == -1 && UString[i]); i++)
	{
		SIVector2D TextureCoord (UString[i] % 16, 15 - UString[i] / 16);
		float TexShiftX = SymbolSize_[UString[i]];
		if (TexShiftX > 1.0f)
			TexShiftX = 1.0f;
		float TexShiftY = 15.0f / 16.0f;
		GL_VERTEX (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (0.0f, 0.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (0, 0)) / 16.0f);
		GL_VERTEX (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (SymbolSize_[UString[i]], 0.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (TexShiftX, 0)) / 16.0f);
		GL_VERTEX (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (SymbolSize_[UString[i]], 1.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (TexShiftX, TexShiftY)) / 16.0f);
		
		GL_VERTEX (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (SymbolSize_[UString[i]], 1.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (TexShiftX, TexShiftY)) / 16.0f);
		GL_VERTEX (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (0.0f, 1.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (0, TexShiftY)) / 16.0f);
		GL_VERTEX (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (0.0f, 0.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (0, 0)) / 16.0f);
		
		TranslateVector.x += SymbolSize_[UString[i]] * Size;
	}

	glEnd();
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
}

#undef GL_VERTEX

float CFont::GetLength(const char* String, float Size, int Start, int End)
{
	ASSERT (Start >= 0 && (End >= 0 || End == -1));
	ASSERT (End > Start || End == -1);
	
	const unsigned char* UString = (const unsigned char*) String;
	Size *= 1.3f;

	float Length = 0.0f;
	for (int i = Start; (End != -1 && i < End) || (End == -1 && UString[i]); i++)
	{
		Length += SymbolSize_[UString[i]] * Size;
	}

	return Length;
}
