#ifndef __CTEXTURE__
#define __CTEXTURE__

class CTexture
{
public:
	
	CTexture (const CTexture& Other);
	CTexture& operator = (const CTexture& Other);

	CTexture();
	~CTexture();
	
	void Generate();
	void Bind();
	void Destroy();
	bool Initialized();
	unsigned ID();

private:
	unsigned ID_;

};

#endif
