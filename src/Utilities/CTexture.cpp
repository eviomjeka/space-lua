﻿#include "CTexture.h"
#include "../Include.h"

CTexture::CTexture() : 
	ID_ (0)
{
}

CTexture::~CTexture()
{}

unsigned CTexture::ID()
{
	return ID_;
}

void CTexture::Generate()
{
	ASSERT (!ID_);
	glGenTextures (1, &ID_);
	ASSERT (ID_);
	glassert();
}

void CTexture::Bind()
{
	ASSERT (ID_);
	glBindTexture (GL_TEXTURE_2D, ID_);
}

void CTexture::Destroy()
{
	ASSERT (ID_);
	glDeleteTextures (1, &ID_);
	glassert();
	ID_ = 0;
}

bool CTexture::Initialized()
{
	return ID_ != 0;
}
