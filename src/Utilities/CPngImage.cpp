﻿#include "CPngImage.h"
#include "../Include.h"

CPngImage CPngImage::Instance;

CPngImage::CPngImage() :
	Data_ (NULL)
{}

CPngImage::~CPngImage()
{
	ASSERT (!Data_);
}

SIVector2D CPngImage::LoadFromFile (const char* FileName, CTexture* Texture, byte** Data, int NumMipMapLevels)
{
	if (NumMipMapLevels == 1)
	{
		char ReadyFileName[MAX_PATH] = "";
		sprintf(ReadyFileName, "textures/%s", FileName);
		CFile File (ReadyFileName, CFILE_READ | CFILE_BINARY);
		Load_ (File);
		File.Destroy();
		
		ASSERT (Texture || Data);
		if (Texture)
		{
			GenTexture_ (Texture, false);
			LoadTexture_();
		}
		if (Data)
			(*Data) = Data_;
		else
		{
			delete[] Data_;
			Data_ = NULL;
		}

		LOG ("Image \"%s\" loaded", FileName);
	}
	else
	{
		ASSERT (Texture);
		GenTexture_ (Texture, true);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, NumMipMapLevels - 1);
		for (int i = NumMipMapLevels - 1; i >= 0; i--)
		{
			char FileName2[MAX_PATH] = "";
			sprintf(FileName2, FileName, i + 1);

			char ReadyFileName[MAX_PATH] = "";
			sprintf(ReadyFileName, "textures/%s", FileName2);

			CFile File (ReadyFileName, CFILE_READ | CFILE_BINARY);
			Load_ (File);
			File.Destroy();
			
			LoadTexture_ (i);
			if (i != 0 || !Data)
			{
				delete[] Data_;
				Data_ = NULL;
			}
			if (i == 0 && Data)
				(*Data) = Data_;

			LOG ("Image \"%s\" loaded", FileName2);
		}
	}
	return Size_;
}

void CPngImage::LoadFromData (byte* Data, CTexture* Texture, SIVector2D Size, int Type)
{
	ASSERT (!Data_);
	Data_ = Data;
	Type_ = Type;
	Size_ = Size;
	
	GenTexture_ (Texture, false);
	LoadTexture_();
	
	Data_ = NULL;
}

void CPngImage::ValidatePng_ (CFile& File)
{
	png_byte Signature[8] = {};
	File.Read (Signature, 8);
	bool IsPng = !png_sig_cmp (Signature, 0, 8);

	ASSERT(IsPng);
}

void CPngImage::ReadData_ (png_structp Png, png_bytep Data, png_size_t Size)
{
	FILE* Source = (FILE*) png_get_io_ptr(Png);
	ASSERT(fread(Data, Size, 1, Source) == 1);
}

CPngImage::tPngAttr CPngImage::GetPngAttr_ (png_structp Png, png_infop Info)
{
	tPngAttr PngAttr =
	{
		png_get_image_width(Png, Info),
		png_get_image_height(Png, Info),
		png_get_bit_depth(Png, Info),
		png_get_color_type(Png, Info),
		png_get_filter_type(Png, Info),
		png_get_compression_type(Png, Info),
		png_get_interlace_type(Png, Info)
	};
	return PngAttr;
}

void CPngImage::Load_ (CFile& File)
{
	ValidatePng_ (File);
	png_structp Png = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!Png)
		error ("Couldn't initialize png read struct");
	png_infop Info = png_create_info_struct (Png);
	if (!Info)
	{
		png_destroy_read_struct(&Png, (png_infopp) 0, (png_infopp) 0);
		error ("Couldn't initialize png info struct");
	}
	png_set_read_fn(Png, (png_voidp) File.GetFILE(), ReadData_);
	png_set_sig_bytes(Png, 8);
	png_read_info(Png, Info);

	tPngAttr PngAttr = GetPngAttr_ (Png, Info);

	int Stride = 0;
	if (PngAttr.ColorType == PNG_COLOR_TYPE_RGBA)
	{
		Type_ = CPNGIMAGE_RGBA;
		Stride = PngAttr.Width * 4;
	}
	else if (PngAttr.ColorType == PNG_COLOR_TYPE_RGB)
	{
		Type_ = CPNGIMAGE_RGB;
		Stride = PngAttr.Width * 3;
	}
	else if (PngAttr.ColorType == PNG_COLOR_TYPE_GRAY)
	{
		Type_ = CPNGIMAGE_GRAYSCALE;
		Stride = PngAttr.Width;
	}
	else
		error ("Unknown image type");

	png_bytep* RowPointers = new png_bytep[PngAttr.Height];
	ASSERT (!Data_);
	Data_ = new byte[PngAttr.Height * Stride];

	Size_.x = PngAttr.Width;
	Size_.y = PngAttr.Height;

	for (size_t i = 0; i < PngAttr.Height; i++)
	{
		int Offset = (PngAttr.Height - i - 1) * Stride;
		RowPointers[i] = (png_bytep) Data_ + Offset;
	}
	png_read_image(Png, RowPointers);
	delete[] RowPointers;
	png_destroy_read_struct(&Png, &Info,(png_infopp) 0);
}

void CPngImage::LoadTexture_ (unsigned MipMapLevel)
{
	for (int x = Size_.x; x > 1; x /= 2)
		ASSERT(!(x % 2));
	for (int y = Size_.y; y > 1; y /= 2)
		ASSERT(!(y % 2));

	if (Type_ == CPNGIMAGE_RGBA)
		glTexImage2D(GL_TEXTURE_2D, MipMapLevel, GL_RGBA, Size_.x, Size_.y,
					 0, GL_RGBA, GL_UNSIGNED_BYTE, Data_);
	else if (Type_ == CPNGIMAGE_RGB)
		glTexImage2D(GL_TEXTURE_2D, MipMapLevel, GL_RGB, Size_.x, Size_.y,
					 0, GL_RGB, GL_UNSIGNED_BYTE, Data_);
	else if (Type_ == CPNGIMAGE_GRAYSCALE)
		glTexImage2D(GL_TEXTURE_2D, MipMapLevel, GL_LUMINANCE, Size_.x, Size_.y,
					 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, Data_);
	else
		error ("Unknown image type");

	glassert();
}

void CPngImage::GenTexture_ (CTexture* Texture, bool MipMapLevels)
{
	Texture->Generate();
	Texture->Bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (MipMapLevels)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	else
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glassert();
}

//==============================================================================

void CPngImage::SaveToFile (const char* FileName, byte* Data, SIVector2D Size, int Type)
{
	ASSERT (!Data_);
	Data_ = Data;
	Type_ = Type;
	Size_ = Size;

	Save_ (FileName);
	Data_ = NULL;
}

void CPngImage::Save_(const char* FileName)
{
	CFile File (FileName, CFILE_WRITE | CFILE_BINARY, false);

	png_bytep* RowPointers = new png_bytep[Size_.y];
	int Stride = 0;
	int ColorType = 0;
	if (Type_ == CPNGIMAGE_RGBA)
	{
		Stride = Size_.x * 4;
		ColorType = PNG_COLOR_TYPE_RGBA;
	}
	else if (Type_ == CPNGIMAGE_RGB)
	{
		Stride = Size_.x * 3;
		ColorType = PNG_COLOR_TYPE_RGB;
	}
	else if (Type_ == CPNGIMAGE_GRAYSCALE)
	{
		Stride = Size_.x;
		ColorType = PNG_COLOR_TYPE_GRAY;
	}
	else
		error ("Unknown image type");

	for (int i = 0; i < Size_.y; i++)
	{
		int Offset = (Size_.y - i - 1) * Stride;
		RowPointers[i] = (png_bytep) Data_ + Offset;
	}

	png_structp Png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	ASSERT(Png);
	png_infop PngInfo = png_create_info_struct(Png);
	ASSERT(PngInfo);

	png_init_io(Png, File.GetFILE());
	png_set_IHDR(Png, PngInfo, Size_.x, Size_.y,
		8, ColorType, PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

	png_write_info(Png, PngInfo);
	png_write_image(Png, RowPointers);
	png_write_end(Png, NULL);

	delete[] RowPointers;
	File.Destroy();
}

void CPngImage::DeleteData()
{
	ASSERT (Data_);
	delete[] Data_;
	Data_ = NULL;
}
