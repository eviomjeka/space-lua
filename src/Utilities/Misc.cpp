﻿#include "Misc.h"

SIVector FloorVector (SFVector f)
{
	return SIVector ((int) floor (f.x), (int) floor (f.y), (int) floor (f.z));
}

int Sign (float f)
{
	if (f < 0.0f)	return -1;
	else			return 1;
}

int Sign (int i)
{
	if (i < 0)	return -1;
	else		return 1;
}

SIVector2D Sign (SIVector2D v)
{
	return SIVector2D(Sign(v.x), Sign(v.y));
}

int Mod(int a, int b)
{
	int r = a % b;
	return r < 0 ? b + r : r;
}

SIVector Mod(SIVector v, int b)
{
	return SIVector(Mod(v.x, b), Mod(v.y, b), Mod(v.z, b));
}

SIVector2D Mod2D(SIVector2D v, int b)
{
	return SIVector2D(Mod(v.x, b), Mod(v.y, b));
}

SIVector ModXZ(SIVector v, int b)
{
	return SIVector(Mod(v.x, b), v.y, Mod(v.z, b));
}

SIVector ModY(SIVector v, int b)
{
	return SIVector(v.x, Mod(v.y, b), v.z);
}

SIVector2D ModXZ2D(SIVector v, int b)
{
	return SIVector2D(Mod(v.x, b), Mod(v.z, b));
}

int Div(int a, int b)
{
	int c = a / b;
	if (a < 0 && a % b) c--;
	return c;
}

SIVector Div(SIVector v, int b)
{
	return SIVector(Div(v.x, b), Div(v.y, b), Div(v.z, b));
}

SIVector2D Div2D(SIVector2D v, int b)
{
	return SIVector2D(Div(v.x, b), Div(v.y, b));
}

SIVector DivXZ(SIVector v, int b)
{
	return SIVector(Div(v.x, b), 0, Div(v.z, b));
}

SIVector DivY(SIVector v, int b)
{
	return SIVector(v.x, Div(v.y, b), v.z);
}

SIVector2D DivXZ2D(SIVector v, int b)
{
	return SIVector2D(Div(v.x, b), Div(v.z, b));
}

SFVector btVector3ToSFVector(btVector3& Vector)
{
	return SFVector(Vector.x(), Vector.y(), Vector.z());
}