#include "CArray.h"
#include "CFile.h"

CFile::CFile() :
File_ (NULL),
Mode_(-1)
{
}

CFile::CFile (const char* FileName, int Mode, bool FileMustExist) 
{
	Init (FileName, Mode, FileMustExist);
}

void CFile::Init (const char* FileName, int Mode, bool FileMustExist)
{
	Mode_ = Mode;

	char GameFolderPath[MAX_PATH] = "";
	CFileSystem::ToGameFolderPath (FileName, GameFolderPath);
	
	CArray<char> ModeArray (4);
	bool RW = ((Mode & CFILE_READ) && (Mode & CFILE_WRITE));
	
	if (!RW)
	{
		if (Mode & CFILE_APPEND)
			ModeArray.Insert ('a');
		else if (Mode & CFILE_WRITE)
			ModeArray.Insert ('w');
		else if (Mode & CFILE_READ)
			ModeArray.Insert ('r');
	}
	else
	{
	   if (Mode & CFILE_CREATE)
			ModeArray.Insert ('w');
	   else
			ModeArray.Insert ('r');
	   ModeArray.Insert ('+');

	}

   if (Mode & CFILE_BINARY)
	   ModeArray.Insert ('b');

	ModeArray.Insert (0);
	ASSERT (ModeArray.Size() != 0);

	File_ = fopen (GameFolderPath, ModeArray.Data());

	if (FileMustExist && !File_)
	{
		error ("Error: Cannot open file \"%s\"", FileName);
	}
}

void CFile::Destroy()
{
	ASSERT (File_);
	fclose (File_);
	File_ = NULL;
}

CFile::~CFile()
{
	ASSERT (!File_);
}

void CFile::Read(void* Data, int Size)
{
	ASSERT (File_);
	ASSERT (Mode_ & CFILE_READ);
	ASSERT (fread (Data, Size, 1, File_) == 1);
}

void CFile::Write(void* Data, int Size)
{
	ASSERT (File_);
	ASSERT (Mode_ & CFILE_WRITE);
	ASSERT (fwrite (Data, Size, 1, File_) == 1);
}

CFile::operator bool ()
{
	return IsFile();
}

bool CFile::operator ! ()
{
	return !IsFile();
}

bool CFile::IsFile()
{
	return File_ != NULL;
}

FILE* CFile::GetFILE()
{
	ASSERT (File_);
	return File_;
}

bool CFile::EndOfFile()
{
	ASSERT (File_);
	return feof (File_) != 0;
}

void CFile::Seek (int Offset, int Origin)
{
	ASSERT (File_);
	ASSERT (fseek (File_, Offset, Origin) == 0);
}

long CFile::Tell()
{
	ASSERT (File_);
	return ftell (File_);
}

void CFile::Flush()
{
	ASSERT (File_);
	ASSERT (fflush (File_) == 0);
}

bool CFile::GetString (char* String, int MaxSize, bool CheckError)
{	
	ASSERT (File_);
	char* RetValue = fgets (String, MaxSize, File_);
	if (CheckError)
	{
		ASSERT (RetValue == String || feof (File_));
		return true;
	}
	else
		return RetValue == String;
}

void CFile::Scan (const char* Format, ...)
{
	va_list Args;
	va_start(Args, Format);
	VScan (Format, Args);
	va_end(Args);
}

void CFile::VScan (const char* Format, va_list Args)
{
	ASSERT (File_);
	vfscanf (File_, Format, Args);
}

void CFile::Print (const char* Format, ...)
{
	va_list Args;
	va_start(Args, Format);
	VPrint (Format, Args);
	va_end(Args);
}

void CFile::VPrint (const char* Format, va_list Args)
{
	ASSERT (File_);
	ASSERT (vfprintf (File_, Format, Args) >= 0);
}

void CFile::ungetChar(char c)
{
	ASSERT(ungetc(c, File_) != EOF);
}
