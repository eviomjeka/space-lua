﻿#ifndef CPNGIMAGE
#define CPNGIMAGE

#include <png.h>
#include "Misc.h"
#include "CTexture.h"
#include "CFile.h"

enum CPngImageColorType
{
	CPNGIMAGE_RGBA, 
	CPNGIMAGE_RGB, 
	CPNGIMAGE_GRAYSCALE
};

class CPngImage
{
private:
	struct tPngAttr
	{
		unsigned Width;
		unsigned Height;
		unsigned BitDepth;
		unsigned ColorType;
		unsigned FilterType;
		unsigned CompressionType;
		unsigned InterlaceType;
	};
	
	byte*		Data_;
	SIVector2D	Size_;
	int			Type_;

	void Load_ (CFile& File);
	void ValidatePng_ (CFile& File);
	tPngAttr GetPngAttr_ (png_structp Png, png_infop Info);
	void LoadTexture_ (unsigned MipMapLevel = 0);
	void Save_ (const char* FileName);
	void GenTexture_ (CTexture* Texture, bool MipMapLevels);

	static void ReadData_ (png_structp Png, png_bytep Data, png_size_t Size);
	
	CPngImage();
	~CPngImage();

public:

	static CPngImage Instance;

	// TODO: byte** -> byte*&?
	SIVector2D LoadFromFile (const char* FileName, CTexture* Texture, byte** Data = NULL, int NumMipMapLevels = 1);
	void LoadFromData (byte* Data, CTexture* Texture, SIVector2D Size, int Type);
	void SaveToFile (const char* FileName, byte* Data, SIVector2D Size, int Type);

	void DeleteData();

};

#endif
