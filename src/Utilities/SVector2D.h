#ifndef __SVECTOR2D__
#define __SVECTOR2D__

#include <cmath>

template <typename T>
struct SVector2D;

typedef SVector2D<int>   SIVector2D;
typedef SVector2D<float> SFVector2D;

template <typename T>
struct SVector2D
{
	T x;
	T y;

	SVector2D()
		: x (0), y (0)
	{}

	explicit SVector2D (const T a)
		: x (a), y (a)

	{}

	SVector2D (const T xx, const T yy)
		: x (xx), y (yy)
	{}
	
	SVector2D<T>& operator += (const SVector2D<T>& Vector)
	{
		x += Vector.x;
		y += Vector.y;

		return *this;
	}

	SVector2D<T>& operator -= (const SVector2D<T>& Vector)
	{
		x -= Vector.x;
		y -= Vector.y;

		return *this;
	}
	
	SVector2D<T>& operator *= (const T N)
	{
		x = (T) (x * N);
		y = (T) (y * N);

		return *this;
	}

	SVector2D<T>& operator /= (const T N)
	{
		x = (T) (x / N);
		y = (T) (y / N);

		return *this;
	}

	void operator = (const SVector2D<T>& Vector)
	{
		x = Vector.x;
		y = Vector.y;
	}

	T SqLen() const
	{
		return x * x + y * y;
	}

	bool InsideOrOnAABB (const SVector2D<T>& A, const SVector2D<T>& B) const
	{
		SVector2D<T> AP = *this - A;
		SVector2D<T> BP = *this - B;

		return (AP.x * BP.x <= 0) && (AP.y * BP.y <= 0);
	}

	SVector2D<T> operator + (const SVector2D<T>& Vector) const
	{
		return SVector2D<T> (x + Vector.x, y + Vector.y);
	}

	SVector2D<T> operator - (const SVector2D<T>& Vector) const
	{
		return SVector2D<T> (x - Vector.x, y - Vector.y);
	}

	SVector2D<T> operator - () const
	{
		return SVector2D<T> (-x, -y);
	}
	
	SVector2D<T> operator * (const T N) const
	{
		return SVector2D<T> ((T) (x * N), (T) (y * N));
	}
	
	SVector2D<T> operator / (const T N) const
	{
		return SVector2D<T> ((T) (x / N), (T) (y / N));
	}

	bool operator == (const SVector2D<T>& Vector) const
	{
		return x == Vector.x && y == Vector.y;
	}

	bool operator != (const SVector2D<T>& Vector) const
	{
		return x != Vector.x || y != Vector.y;
	}
	
	void operator () (T xx, T yy)
	{
		x = xx;
		y = yy;
	}
	
	SVector2D<T> Normalize()
	{
		T Len = (T) sqrtf ((float) SqLen());
		return (*this) / Len;
	}

	template <typename T2>
	operator SVector2D<T2> () const
	{
		return SVector2D<T2> ((T2) x, (T2) y);
	}

	T Dot (const SVector2D<T>& Vector) const
	{
		return x * Vector.x + y * Vector.y;
	}

	SVector2D<T> Abs() const
	{
		return SVector2D<T> (x >= 0 ? x : -x, y >= 0 ? y : -y);
	}

	SIVector2D Sign() const
	{
 		return SIVector2D (
 			((float) x) < 0.0f ? -1 : 1,
 			((float) y) < 0.0f ? -1 : 1);
	}
	
	SVector2D<T> PerComponentProduct (const SVector2D<T>& Vector) const
	{
		return SVector2D<T> (x * Vector.x, y * Vector.y);
	}
	
};

#endif
