#include "CBlockManager.h"
#include "Include.h"
#include "CBlock.h"
#include "CWorld.h"
#include "Blocks/CBlockBody.h"
#include "Blocks/CBlockEngine.h"
#include "Blocks/CBlockMain.h"
#include "Blocks/CBlockRadar.h"
#include "Blocks/CBlockShield.h"
#include "Blocks/CBlockWeapon.h"
#include "Blocks/CBlockHinge.h"

CBlockBase::CBlockBase()
{
}

CBlockBase::~CBlockBase()
{
}

void CBlockBase::setParameters(int id, EBlockType type, float mass, int lives, string name, SIVector2D textureCoord)
{
	id_ = id;
	type_ = type;
	mass_ = mass;
	lives_ = lives;
	name_ = name;
	textureCoord_ = SIVector2D(textureCoord.x, 15 - textureCoord.y);
}

int CBlockBase::getID()
{
    return id_;
}

int CBlockBase::getLives()
{
    return lives_;
}

EBlockType CBlockBase::getType()
{
	return type_;
}

float CBlockBase::getMass()
{
    return mass_;
}

string CBlockBase::getName()
{
	return name_;
}

SIVector2D CBlockBase::getTextureCoord()
{
	return textureCoord_;
}

void CBlockBase::createBlock(CBlock& block, CFile& file)
{
}

CBlock CBlockManager::createBlock(CFile& file, SFVector color)
{
	CBlock block;

	char name[256] = "";
	SIVector position;
	file.Scan("%s (%d, %d, %d)", &name, &position.x, &position.y, &position.z);

	int i;
	bool found = false;
	for (i = 0; i < MAX_BLOCKS; i++)
	{
		if (WORLD->blockManager_->blocks_[i] &&
			WORLD->blockManager_->blocks_[i]->name_ == name)
		{
			found = true;
			break;
		}
	}
	ASSERT(found);

	CBlockBase* blockBase = WORLD->blockManager_->getBlockById(i);

	block.base_ = blockBase;
	block.lives_ = blockBase->lives_;
	block.position_ = position;
	block.color_ = color;
	blockBase->createBlock(block, file);

	if (blockBase->getType() == BlockWeapon || blockBase->getType() == BlockEngine ||
		blockBase->getType() == BlockRadar)
	{
		char name[256] = "";
		file.Scan(" %s", name);
		block.name_ = name;
	}
	
	if (blockBase->getType() == BlockEngine || blockBase->getType() == BlockHinge)
	{
		SIVector orientation;
		file.Scan(" (%d, %d, %d)", &orientation.x, &orientation.y, &orientation.z);
		block.orientation_ = orientation;
	}

	return block;
}

CBlockManager::CBlockManager(const char* database)
{
    CFile db(database, CFILE_READ);

	for (int i = 0; i < 256; i++)
		blocks_.push_back(NULL);

    while (!db.EndOfFile())
    {
        float mass;
        int id, type;
		char name[256];
        int lives;
		SIVector2D textureCoord;
        CBlockBase* currentBlock;
        db.Scan(" %d: (type %d) %s lives: %d mass: %f (%d, %d) ", &id, &type, name, &lives, &mass, 
			&textureCoord.x, &textureCoord.y);
        switch (type)
        {
            case BlockMain:
                currentBlock = new CBlockMain;
                break;

            case BlockBody:
                currentBlock = new CBlockBody;
                break;

            case BlockWeapon:
                int cooldown, damage;
                float bulletSpeed;
                db.Scan(" | cooldown: %d damage: %d speed: %f ", &cooldown, &damage, &bulletSpeed);
                currentBlock = new CBlockWeapon(damage, cooldown, bulletSpeed);
                break;

            case BlockEngine:
                float power;
                db.Scan(" | power: %f ", &power);
                currentBlock = new CBlockEngine(power);
                break;

            case BlockShield:
                currentBlock = new CBlockShield;
                break;

            case BlockRadar:
                float radius;
                db.Scan(" | radius: %f ", &radius);
                currentBlock = new CBlockRadar(radius);
                break;

			case BlockHinge:
				currentBlock = new CBlockHinge;
				break;

			default:
				ASSERT(false);
				break;
        }
		currentBlock->setParameters(id, (EBlockType) type, mass, lives, name, textureCoord);
        blocks_[id] = currentBlock;

		//db.Scan(" ");
    }

    db.Destroy();
}

CBlockBase* CBlockManager::getBlockById(int id)
{
	ASSERT(blocks_[id]);
	return blocks_[id];
}

CBlockManager::~CBlockManager()
{
	for (unsigned i = 0; i < blocks_.size(); i++)
		delete blocks_[i];
}
