#include "CCollisionObject.h"

CCollisionObject::CCollisionObject(tType Type) :
	Type_(Type)
{
}

CCollisionObject::tType CCollisionObject::getType()
{
	return Type_;
}
