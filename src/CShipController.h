#ifndef __CSHIPCONTROLLER__
#define __CSHIPCONTROLLER__

extern "C"
{
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}

#include "CBlock.h"
#include "CShipComponent.h"

#define LUA_ADD_INTEGER(state, num, name)	\
	lua_pushstring(state, name);			\
	lua_pushinteger(state, num);			\
	lua_settable(state, -3);

#define LUA_ADD_STRING(state, str, name)	\
	lua_pushstring(state, name);			\
	lua_pushstring(state, str);				\
	lua_settable(state, -3);

#define LUA_ADD_NUMBER(state, num, name)	\
	lua_pushstring(state, name);			\
	lua_pushnumber(state, num);				\
	lua_settable(state, -3);

#define LUA_ADD_MEMBER(state, obj, memb)	\
	LUA_ADD_NUMBER(state, obj.memb, #memb)

#define LUA_ADD_MEMBER_G(state, obj, memb)	\
	LUA_ADD_NUMBER(state, obj.memb(), #memb)

#define LUA_SET_METATABLE(state, class)		\
	lua_getglobal(state, class);			\
	lua_getfield(state, -1, "metatable");	\
	lua_setmetatable(state, -3);			\
	lua_pop(state, 1);

#define LUA_PUSH_VECTOR(state, vec)			\
	lua_newtable(luaState);					\
	LUA_ADD_MEMBER(state, vec, x)			\
	LUA_ADD_MEMBER(state, vec, y)			\
	LUA_ADD_MEMBER(state, vec, z)			\
	LUA_SET_METATABLE(state, "Vector3");

#define LUA_ADD_VECTOR(state, vec, name)	\
	lua_pushstring(state, name);			\
	LUA_PUSH_VECTOR(state, vec)				\
	lua_settable(state, -3);

#define LUA_PUSH_BT_VECTOR(state, vec)		\
	lua_newtable(luaState);					\
	LUA_ADD_MEMBER_G(state, vec, x)			\
	LUA_ADD_MEMBER_G(state, vec, y)			\
	LUA_ADD_MEMBER_G(state, vec, z)			\
	LUA_SET_METATABLE(state, "Vector3");

#define LUA_ADD_BT_VECTOR(state, vec, name)	\
	lua_pushstring(state, name);			\
	LUA_PUSH_BT_VECTOR(state, vec)			\
	lua_settable(state, -3);

#define LUA_PUSH_MATRIX(state, n, m)				\
{													\
	btMatrix3x3 __m = m;							\
													\
	lua_newtable(state);							\
	lua_pushstring(state, "data");					\
	lua_newtable(state);							\
	for (int __i = 0; __i < n; __i++)				\
	{												\
		lua_newtable(state);						\
													\
		for (int __j = 0; __j < n; __j++)			\
		{											\
			lua_pushnumber(state, __m[__i][__j]);	\
        	lua_rawseti(luaState, -2, __j + 1);		\
		}											\
													\
        lua_rawseti(state, -2, __i + 1);			\
	}												\
													\
	lua_settable(state, -3);						\
													\
	LUA_SET_METATABLE(state, "Matrix")				\
}

#define LUA_ADD_MATRIX(state, n, m, name)	\
	lua_pushstring(state, name);			\
	LUA_PUSH_MATRIX(state, n, m)			\
	lua_settable(state, -3);

#define LUA_REGISTER_FUNCTION(state, method, func)			\
	lua_register(state, func,								\
		[](lua_State* luaState)								\
		{													\
			CShipController* shipController =				\
				CShipController::getByLuaState(luaState);	\
			ASSERT(shipController);							\
			return shipController->method();				\
		}													\
		);

class CShip;

class CShipController
{
private:
	CShip*					parent_;
	lua_State*				luaState_;
	lua_State*				luaThreadState_;
	bool					completedStep_;

	static char				this_key_;

	CBlock* getBlock_(int param, CShipComponent** _component = NULL);
	btVector3 getVector_(int param);

public:
	CShipController(CShip* parent, const char* script);
	~CShipController();

	void stop();
	bool isRunning();
	void doStep(float dt);
	void registerBlock(int component, int block, string name);
	int dumpShip();

	int yield();
	int shoot();
	int setEnginePower();
	int readRadar();


	static CShipController* getByLuaState(lua_State* luaState);

	friend void yield_break(lua_State* luaState, lua_Debug*);
};

#endif