#include "CParticleSystem.h"
#include "Utilities/CPngImage.h"
#include "CBlock.h"
#include "CShip.h"
#include "Blocks/CBlockEngine.h"
#include "CWorld.h"

void CParticleSystem::Init()
{
	CTexture t;
	CPngImage::Instance.LoadFromFile("particle.png", &t);
	texture_ = t.ID();
	glBindTexture(GL_TEXTURE_2D, texture_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE); // TODO: for text
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

	for (int i = 0; i < MAX_ENGINES; i++)
		fire_[i].exist = false;
	for (int i = 0; i < MAX_EXPLOSIONS; i++)
		explosion_[i].exist = false;
}

void CParticleSystem::Destroy()
{
	glDeleteTextures(1, &texture_);
}

void CParticleSystem::addEngine(CShipComponent* shipComponent, CBlock* block)
{
#ifndef NO_PARTICLES
	bool found = false;
	int i;

	for (i = 0; i < MAX_ENGINES; i++)
		if (!fire_[i].exist)
		{
			found = true;
			break;
		}

	ASSERT(found);
	fire_[i].exist = true;
	fire_[i].shipComponent = shipComponent;
	fire_[i].block = block;

	for (int j = 0; j < MAX_FIRE_PARTICLES; j++)
	{
		fire_[i].particle[j].active = false;
		fire_[i].particle[j].maxLife = 0.5f;
		fire_[i].particle[j].life = j * 1000.0f / ((float) MAX_FIRE_PARTICLES) * ((float) PARTICLE_FADE) / 2.0f;
		// TODO: activate in time (if we skip frames)
	}
#endif
}

void CParticleSystem::addExplosion(CShipComponent* shipComponent, CBlock* block)
{
#ifndef NO_PARTICLES
	bool found = false;
	int i;

	for (i = 0; i < MAX_EXPLOSIONS; i++)
		if (!explosion_[i].exist)
		{
			found = true;
			break;
		}

	ASSERT(found);
	explosion_[i].exist = true;
	explosion_[i].shipComponent = shipComponent;
	explosion_[i].block = block;

	btVector3 btPosition = explosion_[i].shipComponent->toWorldPosition(explosion_[i].block->getPosition());
	btVector3 btVelocity = explosion_[i].shipComponent->toWorldVelocity(explosion_[i].block->getPosition());

	SFVector velocity = SFVector(btVelocity.getX(), btVelocity.getY(), btVelocity.getZ());
	SFVector position = SFVector(btPosition.getX(), btPosition.getY(), btPosition.getZ());
	
	for (int j = 0; j < MAX_EXPLOSION_PARTICLES; j++)
	{
		explosion_[i].particle[j].active = true;
		explosion_[i].particle[j].life = 2.0f + ((rand() / ((float) RAND_MAX)) - 0.5f) * 0.8f;
		explosion_[i].particle[j].maxLife = explosion_[i].particle[j].life;

		SFVector randVector;
		for (;;)
		{
			randVector = SFVector(((rand() / ((float) RAND_MAX)) - 0.5f) * 2.0f, 
								  ((rand() / ((float) RAND_MAX)) - 0.5f) * 2.0f, 
								  ((rand() / ((float) RAND_MAX)) - 0.5f) * 2.0f);
			if (randVector.SqLen() > 1.0f * 1.0f)
				continue;
			break;
		}
		SFVector randVector2;
		for (;;)
		{
			randVector2 = SFVector(((rand() / ((float) RAND_MAX)) - 0.5f) * 2.0f, 
								   ((rand() / ((float) RAND_MAX)) - 0.5f) * 2.0f, 
								   ((rand() / ((float) RAND_MAX)) - 0.5f) * 2.0f);
			if (randVector2.SqLen() > 1.0f * 1.0f)
				continue;
			break;
		}

		explosion_[i].particle[j].position = position + randVector / 2.0f;
		explosion_[i].particle[j].velocity = velocity + randVector2 * 1.7f;
	}
#endif
}

void CParticleSystem::removeShipEngines(CShipComponent* shipComponent)
{
#ifndef NO_PARTICLES
	for (int i = 0; i < MAX_ENGINES; i++)
	{
		if (fire_[i].exist && fire_[i].shipComponent == shipComponent)
			fire_[i].exist = false;
	}
#endif
}

void CParticleSystem::addBullet(CBullet* bullet)
{
#ifndef NO_PARTICLES
	int n = bullet->n();
	for (int i = 0; i < MAX_BULLET_PARTICLES; i++)
	{
		bullet_[n].particle[i].active = false;
		bullet_[n].particle[i].maxLife = 1.0f;
		bullet_[n].particle[i].life = i * 1000.0f / ((float) MAX_BULLET_PARTICLES) * ((float) PARTICLE_FADE);
		// TODO: activate in time (if we skip frames)
	}
#endif
}
void CParticleSystem::Proc(int Time)
{
#ifndef NO_PARTICLES

	float modelview[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview);

// TODO: enable lighing
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBindTexture(GL_TEXTURE_2D, texture_);

	glBegin(GL_QUADS);

	for (int i = 0; i < MAX_ENGINES; i++)
	{
		if (!fire_[i].exist)
			continue;

		if (!fire_[i].block->exist())
		{
			fire_[i].exist = false;
			continue;
		}

		if (fire_[i].block->getPower() == 0) // TODO: wait for particles
		{
			for (int j = 0; j < MAX_FIRE_PARTICLES; j++)
			{
				fire_[i].particle[j].active = false;
				fire_[i].particle[j].life = j * 1000.0f / ((float) MAX_FIRE_PARTICLES) * ((float) PARTICLE_FADE) / 2.0f;
			}
			continue;
		}

		btVector3 btPosition = fire_[i].shipComponent->toWorldPosition(fire_[i].block->getPosition());
		btVector3 btVelocity = fire_[i].shipComponent->toWorldVelocity(fire_[i].block->getPosition());
		
		SFVector velocity = SFVector(btVelocity.getX(), btVelocity.getY(), btVelocity.getZ()) * 
			fabs(fire_[i].block->getPower()) / 100.0f;
		SFVector position = SFVector(btPosition.getX(), btPosition.getY(), btPosition.getZ());

		for (int j = 0; j < MAX_FIRE_PARTICLES; j++)
		{
			SFVector color;
			if (fire_[i].particle[j].life >= 0.25f)
				color = SFVector(0.3f, 0.3f, 1.0f) * (2.0f * fire_[i].particle[j].life - 0.5f) / 0.2f + 
						SFVector(1.0f, 0.7f, 0.3f) * (1.0f - (2.0f * fire_[i].particle[j].life - 0.5f) / 0.2f);
			else if (fire_[i].particle[j].life <= 0.1f)
				color = SFVector(1.0f, 0.7f, 0.3f) * (2.0f * fire_[i].particle[j].life - 0.8f) / 0.2f + 
						SFVector(1.0f, 1.0f, 0.8f) * (1.0f - (2.0f * fire_[i].particle[j].life - 0.8f) / 0.2f);
			else
				color = SFVector(1.0f, 0.7f, 0.3f);

			updateParticle_(modelview, Time, 0.2f, fire_[i].particle[j], color);

			btVector3 btOrientation = fire_[i].shipComponent->toWorldDirection(fire_[i].block->getOrientation());
			SFVector orientation = SFVector(btOrientation.getX(), btOrientation.getY(), btOrientation.getZ());

			if (fire_[i].particle[j].life < 0.0f)
			{
				fire_[i].particle[j].active = true;
				fire_[i].particle[j].life += 0.5f;
				fire_[i].particle[j].position = position - orientation * 0.5f;
				fire_[i].particle[j].velocity = SFVector ((((float) (rand() % 51)) - 25.0f) / 15.0f, 
												  (((float) (rand() % 51)) - 25.0f) / 15.0f, 
												  (((float) (rand() % 51)) - 25.0f) / 15.0f) * 0.5f * 2.0f + 
					velocity - orientation * log(1.0f + fire_[i].block->getPower()) * 3.0f; // TODO: fix coef
			}
		}
	}

	for (int i = 0; i < MAX_EXPLOSIONS; i++)
	{
		if (!explosion_[i].exist)
			continue;

		bool exist = false;
		for (int j = 0; j < MAX_EXPLOSION_PARTICLES; j++)
		{
			SFVector color = SFVector(1.0f, 0.7f, 0.3f);

			updateParticle_(modelview, Time, 0.3f, explosion_[i].particle[j], color);

			if (explosion_[i].particle[j].life < 0.0f)
				explosion_[i].particle[j].active = false;
			else
				exist = true;
		}
		if (!exist)
			explosion_[i].exist = false;
	}

	for (int i = 0; i < MAX_BULLETS; i++)
	{
		if (!WORLD->bullets_[i].exist())
			continue;

		btVector3 btPosition = WORLD->bullets_[i].body_->getCenterOfMassPosition();
		btVector3 btVelocity = WORLD->bullets_[i].body_->getLinearVelocity();
		
		SFVector velocity = SFVector(btVelocity.getX(), btVelocity.getY(), btVelocity.getZ());
		SFVector position = SFVector(btPosition.getX(), btPosition.getY(), btPosition.getZ());

		for (int j = 0; j < MAX_BULLET_PARTICLES; j++)
		{
			SFVector color = WORLD->bullets_[i].color() + SFVector(0.4f);

			if (color.x > 1.0f)
				color.x = 1.0f;
			if (color.y > 1.0f)
				color.y = 1.0f;
			if (color.z > 1.0f)
				color.z = 1.0f;

			updateParticle_(modelview, Time, 0.4f, bullet_[i].particle[j], color);

			if (bullet_[i].particle[j].life < 0.0f)
			{
				bullet_[i].particle[j].active = true;
				bullet_[i].particle[j].life += 1.0f; // TODO: if < -1.0f?
				bullet_[i].particle[j].position = position; // TODO: + vt + at^2/2
				SFVector randVector(((rand() / ((float) RAND_MAX)) - 0.5f) * 2.5f, 
									((rand() / ((float) RAND_MAX)) - 0.5f) * 2.5f, 
									((rand() / ((float) RAND_MAX)) - 0.5f) * 2.5f);
				bullet_[i].particle[j].velocity = velocity + randVector;
			}
		}
	}

	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

#endif
}

void CParticleSystem::updateParticle_(float modelview[16], int Time, 
	float size, SParticle& particle, SFVector color)
{
	if (particle.active)
	{
		glColor4f(color.x, color.y, color.z, particle.life / particle.maxLife);
		SFVector DirX = SFVector (modelview[0], modelview[4], modelview[8]);
		SFVector DirY = SFVector (modelview[1], modelview[5], modelview[9]);

		SFVector v[4] = {};

		v[0] = particle.position + (-DirX - DirY) * size;
		v[1] = particle.position + ( DirX - DirY) * size;
		v[2] = particle.position + ( DirX + DirY) * size;
		v[3] = particle.position + (-DirX + DirY) * size;
		
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(v[0].x, v[0].y, v[0].z);
		
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(v[1].x, v[1].y, v[1].z);
		
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(v[2].x, v[2].y, v[2].z);
		
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(v[3].x, v[3].y, v[3].z);

		particle.position += particle.velocity * ((float) Time) / 1000.0f;
	}

	particle.life -= PARTICLE_FADE * Time;
}
