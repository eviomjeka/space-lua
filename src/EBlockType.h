#ifndef EBLOCKTYPE
#define EBLOCKTYPE

enum EBlockType
{
    BlockMain = 1,
    BlockBody = 2,
    BlockWeapon = 3,
    BlockEngine = 4,
    BlockRadar = 5,
    BlockShield = 6,
	BlockHinge = 7
};

#endif // EBLOCKTYPE
