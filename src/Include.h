#ifndef __INCLUDE_FILE__
#define __INCLUDE_FILE__

#define APPLICATION_NAME "Space LUA"

#define ASSERTS ASSERTS_NORMAL

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <array>
#include <cmath>
#include <cstring>
#include <cstdarg>

using std::flush;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;
using std::queue;
using std::array;
using std::min;
using std::max;

#ifdef _MSC_VER // WINDOWS

	#include "Platform/Windows/CFileSystem.h"
	#include "Platform/Windows/CMutex.h"
	#include "Platform/Windows/CWindow.h"
	#include "Platform/Windows/Error.h"
	#include "Platform/Windows/PlatformMisc.h"

#else // LINUX

	#include "Platform/Linux/CFileSystem.h"
	#include "Platform/Linux/CMutex.h"
	#include "Platform/Linux/CWindow.h"
	#include "Platform/Linux/Error.h"
	#include "Platform/Linux/PlatformMisc.h"

#endif

#include "Utilities/CLog.h"
#include "Utilities/SVector.h"
#include "Utilities/SVector2D.h"

#endif // __INCLUDE_FILE__
