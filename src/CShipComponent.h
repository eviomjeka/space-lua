#ifndef __CSHIPCOMPONENT__
#define __CSHIPCOMPONENT__

#include <btBulletDynamicsCommon.h>
#include "Include.h"
#include "CCollisionObject.h"
#include "CBlock.h"
#include "Utilities/CFile.h"
#include "CBullet.h"

struct lua_State;
class CShip;

class CShipComponent : public CCollisionObject
{
private:
	struct tConstraint
	{
		CShipComponent* parent;
		CShipComponent* child;
		unsigned parentBlock;
		unsigned childBlock;
	};

	CShip*					parent_;
	int						ID_;
	string					name_;
	vector<CBlock>			blocks_;
	SFVector				color_;
	int						mainBlock_;
	bool					shapeUpdated_;
	vector<tConstraint>		constraints_;

	btTransform				principalTransform_;
	btTransform				principalTransformInverse_;
	btCompoundShape*		shape_;
	btDefaultMotionState*	motionState_;
	btRigidBody*			body_;


	void readShape_(CFile& construction);
	void createBody_(const btTransform& transform);
	void buildShape_(btScalar& mass, btVector3& inertia);
	void rebuildShape_();
	void removeOrphanedBlocks_();
	void destroyingBlock_(CBlock* block);
	void removeConstraints_();

	static void addConstraint_(const tConstraint& constraint);
public:
	BT_DECLARE_ALIGNED_ALLOCATOR()

	CShipComponent(CShip* parent, int ID, string name, CFile& construction, const btTransform& transform, SFVector color);
	~CShipComponent();

	CShipComponent(const CShipComponent&);
	CShipComponent& operator =(CShipComponent&);

	void draw();
	void connectHinge(SIVector attachmentPoint, CShipComponent* hingeComponent, SIVector hingeBlock);
	void hit(CBullet* bullet, const btVector3& pointA, const btVector3& pointB);
	void die();
	void prepareForPhysicsStep();
	void dumpState(lua_State* luaState);

	CBlock* getBlockByWorldPositionClosest_(const btVector3&  pointA, const btVector3& pointB);
	btVector3 toWorldDirection(const btVector3& direction);
	btVector3 toWorldPosition(const btVector3& localPosition = btVector3(0.0f, 0.0f, 0.0f));
	btVector3 toWorldPositionRelative(const btVector3& localPosition = btVector3(0.0f, 0.0f, 0.0f));
	btVector3 toWorldVelocity(const btVector3& localPosition, const btVector3& localVelocity = btVector3(0.0f, 0.0f, 0.0f));
	void applyForceLocal(const btVector3& force, const btVector3& position);

	CBlock* getBlock(unsigned ID);
	CBlock* getMainBlock();
	CBlock* getBlockByLocalPosition(const SIVector& position);
	CBlock* getBlockByWorldPosition(const btVector3& position);
	CShip* getParent();
	string getName();

	bool doesExist();
};


#endif
