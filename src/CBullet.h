#ifndef CBULLET
#define CBULLET

#include "Utilities/SVector.h"
#include <btBulletDynamicsCommon.h>
#include "CCollisionObject.h"

#define DESPAWN_DISTANCE	100
#define BULLET_RADIUS		0.2f
#define BULLET_SPEED		30.0f
#define BULLET_MASS			0.00005f
#define MAX_BULLETS			10000

class CBullet : public CCollisionObject
{
public:
	
	CBullet();
	void Init(btVector3& position, btVector3& velocity, SFVector color, int team);

	void doStep(float dt);
	void draw();
	void setN(int n);

	bool exist();
	void die();
	void checkDespawnDistance();
	void checkExistance();
	void hit();
	int team();
	SFVector color();
	int n();

private:

	btDefaultMotionState*	motionState_;
	btRigidBody*			body_;
	SFVector				color_;
	bool					hit_;
	int						team_;
	int						n_;
	
	friend class CParticleSystem;

};

#endif // CBULLET
