#ifndef CBLOCKMANAGER
#define CBLOCKMANAGER

#include "Utilities/SVector.h"
#include "Utilities/CFile.h"
#include "EBlockType.h"
#include <vector>
#include <string>

class CBlock;

using std::vector;
using std::string;

#define MAX_BLOCKS 256

class CBlockManager;

class CBlockBase
{
public:
    CBlockBase();
	virtual ~CBlockBase();
    
	void setParameters(int id, EBlockType type, float mass, int lives, string name, SIVector2D textureCoord);

    int getLives();
    int getID();
    float getMass();
	EBlockType getType();
	string getName();
    SIVector2D getTextureCoord();

	virtual void createBlock(CBlock& block, CFile& file);

private:

	EBlockType type_;
	string name_;
    int id_, lives_;
    float mass_;
	SIVector2D textureCoord_;

	friend class CBlockManager;

};

class CBlockManager
{
public:
    CBlockManager(const char* database);
	virtual ~CBlockManager();
    CBlockBase* getBlockById(int id);

	CBlock createBlock(CFile& file, SFVector color);

private:
    vector<CBlockBase*> blocks_;

	friend class CBlockBase;

};

#endif // CBLOCKMANAGER
