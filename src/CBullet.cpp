#include "CBullet.h"
#include "Include.h"
#include "CWorld.h"

CBullet::CBullet() :
	CCollisionObject(eBullet),
	motionState_(NULL),
	body_(NULL),
	team_(0)
{
	ASSERT(!exist());
}

void CBullet::Init(btVector3& position, btVector3& velocity, SFVector color, int team)
{	
	color_ = color;
	hit_ = false;
	team_ = team;

	btVector3 startingPosition = position + 1.1f * velocity.normalized() * sqrt(3.0f) / 2.0f;
	motionState_ = new btDefaultMotionState(btTransform(btMatrix3x3::getIdentity(), startingPosition));
	btRigidBody::btRigidBodyConstructionInfo rbInfo(BULLET_MASS, motionState_, WORLD->sphereShape_, btVector3(0, 0, 0));
	body_ = new btRigidBody(rbInfo);
	body_->setUserPointer(this);
	body_->setLinearVelocity(velocity);
	WORLD->world_->addRigidBody(body_);

	WORLD->particleSystem_->addBullet(this);
}

void CBullet::setN(int n)
{
	n_ = n;
}

void CBullet::draw()
{
	ASSERT(exist());

#ifdef NO_PARICLES

	glPushMatrix();
	btTransform bulletTransform = body_->getWorldTransform();
	btScalar glMatrix[16];
	bulletTransform.getOpenGLMatrix(glMatrix);
	glMultMatrixf(glMatrix);

	float size = BULLET_RADIUS;

	SFVector color = color_ + SFVector (0.4f);

	if (color.x > 1.0f)
		color.x = 1.0f;
	if (color.y > 1.0f)
		color.y = 1.0f;
	if (color.z > 1.0f)
		color.z = 1.0f;

	glColor3f(color.x, color.y, color.z);
	float R = size, s1 = 5, s2 = 5;
	float x1 = 0, y1 = 0, z1 = 0, x2 = 0, y2 = 0, z2 = 0;
	for (int i = 0; i < s1; i++)
	{
		glEnable(GL_NORMALIZE);
		glBegin(GL_TRIANGLE_STRIP);
		for (int j = 0; j <= s2; j++)
		{
			x1 = R * sin(-2.0f * M_PI * j / s2) * sin(M_PI * i / s1);
			x2 = R * sin(-2.0f * M_PI * j / s2) * sin(M_PI * (i + 1) / s1);
			y1 = R * cos(-2.0f * M_PI * j / s2) * sin(M_PI * i / s1);
			y2 = R * cos(-2.0f * M_PI * j / s2) * sin(M_PI * (i + 1) / s1);
			z1 = R * cos(M_PI * i / s1);
			z2 = R * cos(M_PI * (i + 1) / s1);

			glNormal3f(x1 / R, y1 / R, z1 / R);
			glTexCoord2f(j/s2, i/s1);
			glVertex3f(x1, y1, z1);
			
			glNormal3f(x2 / R, y2 / R, z2 / R);
			glTexCoord2f(j/s2, (i+1)/s1);
			glVertex3f(x2, y2, z2);
		}
		glEnd();
	}
	
	glPopMatrix();
#endif
}

void CBullet::checkExistance()
{
	if (exist() && (hit_ || body_->getCenterOfMassPosition().length2() > DESPAWN_DISTANCE * DESPAWN_DISTANCE))
		die();
}

bool CBullet::exist()
{
	return body_ != NULL;
}

void CBullet::hit()
{
	hit_ = true;
}

void CBullet::die()
{
	WORLD->world_->removeRigidBody(body_);
	delete body_;
	body_ = NULL;
	delete motionState_;
	motionState_ = NULL;
}

int CBullet::team()
{
	return team_;
}

SFVector CBullet::color()
{
	return color_;
}

int CBullet::n()
{
	return n_;
}
