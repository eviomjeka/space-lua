#ifndef CPARTICLESYSTEM
#define CPARTICLESYSTEM

#include "Include.h"
#include "CBullet.h"
#include "CShip.h"

#define MAX_FIRE_PARTICLES 300
#define MAX_BULLET_PARTICLES 30
#define MAX_EXPLOSION_PARTICLES 1000

#define PARTICLE_FADE 0.002f

#define MAX_ENGINES 1024
#define MAX_EXPLOSIONS 256

class CShipComponent;
class CBlock;

struct SParticle
{
	bool active;
	float life;
	float maxLife;
	SFVector position;
	SFVector velocity;
};

struct SEngineFire
{
	bool exist;
	SParticle particle[MAX_FIRE_PARTICLES];
	CShipComponent* shipComponent;
	CBlock* block;
};

struct SPlasmaBullet
{
	SParticle particle[MAX_BULLET_PARTICLES];
};

struct SExplosion
{
	bool exist;
	SParticle particle[MAX_EXPLOSION_PARTICLES];
	CShipComponent* shipComponent;
	CBlock* block;
};

class CParticleSystem
{
public:

	void Init();
	void Destroy();
	void Proc(int Time);

	void addEngine(CShipComponent* shipComponent, CBlock* block);
	void removeShipEngines(CShipComponent* shipComponent);
	void addExplosion(CShipComponent* shipComponent, CBlock* block);
	void addBullet(CBullet* bullet);

private:

	void updateParticle_(float modelview[16], int Time, float size, SParticle& particle, SFVector color);

	unsigned texture_;
	SEngineFire fire_[MAX_ENGINES];
	SPlasmaBullet bullet_[MAX_BULLETS];
	SExplosion explosion_[MAX_EXPLOSIONS];

};

#endif // CPARTICLESYSTEM
